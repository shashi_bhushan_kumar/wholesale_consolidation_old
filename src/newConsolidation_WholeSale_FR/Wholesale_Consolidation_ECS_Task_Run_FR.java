package newConsolidation_WholeSale_FR;

import static org.junit.Assert.*;

import java.io.FileOutputStream;
import java.io.IOException;



import java.util.HashMap;
import java.util.concurrent.TimeUnit;






import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import SandPiper_I5a_All_CommonFunctions.SandPiper_I5a_Utilities;
import Utilities_i5A_All.MyException;
import Utilities_i5A_All.ProjectConfigurations;
import Utilities_i5A_All.Reporting_Utilities;
import Utilities_i5A_All.RowGenerator;
import Utilities_i5A_All.getCurrentDate;
import Utilities_i5A_All.utilityFileWriteOP;


public class Wholesale_Consolidation_ECS_Task_Run_FR {
	

			String DriverPath;
			String DriverName;
			String DriverType;
			String BrowserPath;
			String ServerName;
			String SheetName;
			String ItemDetailsSheetName;
			
			String TestDataPath;
			String TemporaryFilePath;
			String Screenshotpath;
			String ResultPath="";
			String OrderDetailsSheetName;
			
			public WebDriver driver = null;
			public WebDriverWait wait = null;
			
			
			ChromeOptions options=null;
			
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	@Before
	public void setUp() throws Exception {
		
		
		
		DriverPath=ProjectConfigurations.LoadProperties("McColls_I5a_DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("McColls_I5a_DriverName");
		DriverType=ProjectConfigurations.LoadProperties("McColls_I5a_DriverType");
		BrowserPath=ProjectConfigurations.LoadProperties("McColls_I5a_BrowserPath");
		ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");
		TestDataPath=ProjectConfigurations.LoadProperties("McColls_I5a_TestDataPath_FR");
		SheetName=ProjectConfigurations.LoadProperties("McColls_I5a_SheetName");
//		ItemDetailsSheetName=ProjectConfigurations.LoadProperties("SandPiper_I5a_ItemDetailsSheetName");
		TemporaryFilePath=ProjectConfigurations.LoadProperties("McColls_I5a_TemporaryFilePath");
		
		
		
		
		System.out.println(BrowserPath);
		
		
		
		
		
		if(ServerName.equalsIgnoreCase("Server1")){
			
			ResultPath=utilityFileWriteOP.ReadResultPathServer1();
		}
		
		
		if(ServerName.equalsIgnoreCase("Server2")){
			
			ResultPath=utilityFileWriteOP.ReadResultPathServer2();
			
		}
		
		
		utilityFileWriteOP.writeToLog("*********************************START**********************************",ResultPath);	 					 
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);
		
		System.setProperty(DriverName, DriverPath);
	}

	@After
	public void tearDown() throws Exception {
		
		utilityFileWriteOP.writeToLog("*********************************END**********************************",ResultPath);	 					 
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime(),ResultPath);
	}

	@Test
	public void test() throws IOException {

		String TestCaseNo = "ECS Job Run";
		String TestCaseName = "ECS Job Run";

		String Final_Result = null;
	
		String ChromeBrowserExePath=null;

		
		String AwsUsername = null;
		String Awspassword = null;
		String Searchkey = null;
		String jobname = null;

		
		//--------------------Doc File---------------------
		XWPFDocument doc = new XWPFDocument();
        XWPFParagraph p = doc.createParagraph();
        XWPFRun xwpfRun = p.createRun();
        
        String consolidatedScreenshotpath="";
        //-------------------------------------------------
		

		try{
			

			AwsUsername = ProjectConfigurations.LoadProperties("SandPiper_I5a_AWS_Username");
			Awspassword= ProjectConfigurations.LoadProperties("SandPiper_I5a_AWS_Password");
			Searchkey = ProjectConfigurations.LoadProperties("SandPiper_I5a_AWS_SearchKey");
			jobname = ProjectConfigurations.LoadProperties("SandPiper_I5a_AWS_Jobname");
			ChromeBrowserExePath=ProjectConfigurations.LoadProperties("ChromeBrowserExePath");
		

				 boolean login = true;
				 boolean taskrun = true;
				 boolean logut = true;
			
				 consolidatedScreenshotpath=ResultPath+"Screenshot/"+TestCaseNo+"/"+TestCaseNo+"_"; 
					//-------------------HTML Header--------------------
				 
					Reporting_Utilities.writeHeaderToHTMLLog(TestCaseNo, TestCaseName, ResultPath);
										
					//----------------------------------------------------

							//driver.get("https://www.google.com");
						/*	
						HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
							chromePrefs.put("profile.default_content_settings.popups", 0);
							chromePrefs.put("download.prompt_for_download", "false");
							chromePrefs.put("download.", "false");
							ChromeOptions options = new ChromeOptions();
							
							options.setBinary(ChromeBrowserExePath);  // setup chrome binary path

							options.setExperimentalOption("prefs", chromePrefs);
							options.setExperimentalOption("useAutomationExtension", false);
							
							driver = new ChromeDriver(options);

							wait=new WebDriverWait(driver, 30);			
							driver.manage().deleteAllCookies();
							driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
							
							driver.get("https://rax-598f5024ec80460190f52fc91b01825a.signin.aws.amazon.com/console");
							*/
					
					HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
					chromePrefs.put("profile.default_content_settings.popups", 0);
					chromePrefs.put("download.prompt_for_download", "false");
					chromePrefs.put("download.", "false");
					
					ChromeOptions options = new ChromeOptions();
					options.setExperimentalOption("prefs", chromePrefs);
					options.setExperimentalOption("useAutomationExtension", false);
					DesiredCapabilities cap = DesiredCapabilities.chrome();
					cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
					cap.setCapability(ChromeOptions.CAPABILITY, options);
					driver = new ChromeDriver(cap);
					wait=new WebDriverWait(driver, 60);			
					driver.manage().deleteAllCookies();
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
					
							driver.get("https://rax-598f5024ec80460190f52fc91b01825a.signin.aws.amazon.com/console");
						
							driver.manage().window().maximize();
               
							AwsUsername="usr-glob-n-mo-testautomation-user-002";
									
							Awspassword="]S1oeO-|Xju7";
									
									
							boolean AwsLogin = SandPiper_I5a_Utilities.aws_login(driver, wait, TestCaseNo, AwsUsername, Awspassword, Screenshotpath, ResultPath,xwpfRun);
							
							 if(AwsLogin==true) {
								 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "AWS LOGIN", "AWS Logged In Successfully.", "PASS", ResultPath);
								}
										
							else{
								Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "AWS LOGIN", "Error Occured during AWS Login.", "FAIL", ResultPath);
							throw new MyException("Test Stopped Because of Failure. Please check Execution log");
							}
							login = login && AwsLogin;
							
							
							String[] taskid=new String[1];
							String taskID="";
							boolean LambdaJobRun = SandPiper_I5a_Utilities.Ecs_task_runner_job_trigger_task_ID_Retrieve(driver, wait, TestCaseNo, Searchkey, jobname, ResultPath, Screenshotpath,xwpfRun,taskid);
							
							 if(LambdaJobRun==true) {
								 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "ECS Task Runner JOB", "ECS Task Runner JOB triggered Successfully.", "PASS", ResultPath);
								 System.out.println("Outsidecaller script taskid "+taskid[0]);
								 taskID=taskid[0];
							 }
										
							else{
								Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "ECS Task Runner JOB", "Error Occured during ECS Task Runner JOB.", "FAIL", ResultPath);
							throw new MyException("Test Stopped Because of Failure. Please check Execution log");
							}
							
							
							taskrun = taskrun && LambdaJobRun;
							
							
							driver.quit();
							
			 
				 
				 if( (login) && (taskrun) ) {
					 
					 Final_Result = "PASS";

					 
					 Assert.assertTrue(TestCaseName, true);
				 }
				 else {
					 Final_Result = "FAIL";
					 
					 Assert.assertTrue(TestCaseName, false);
					 
				 }
		}
		
		
		catch(Exception e) {
			e.printStackTrace();

			 Final_Result = "FAIL";
			 
			 Assert.assertTrue(TestCaseName, false);
		}
		finally
		 {   
			FileOutputStream out1 = new FileOutputStream(consolidatedScreenshotpath+Final_Result+"1.docx");
			doc.write(out1);
		        out1.close();
		        doc.close();
			 //Reporting_Utilities.writeCloseRowToHTMLLog(TestCaseNo, ResultPath);
		        RowGenerator.collateHTMLLog(TestCaseNo, ResultPath);
		 }
			
		}
}
