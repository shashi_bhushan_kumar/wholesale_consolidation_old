package McColls_I5a_All_CommonFunctions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.commons.io.FileUtils;

import Utilities_i5A_All.utilityFileWriteOP;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class McColls_I5a_Utilities_NAS {
	
public static ArrayList<HashMap<String, String>> TemporaryOrderIDExtract(String SFTPHostName,
		int SFTPPort,String SFTPUserName,String SFTPPassword,String NasPath,String LocalPath,
		
		String OrderID,	String Prefix,String shipToLocationId,String ordDate,String Quantities,
		String items,String tcid
             ) throws IOException {
	
	
	
		
	int itemCount=0;
	/*String SFTPHostName="tgmus03x"; 
	int SFTPPort=22; 
	String SFTPUserName="mwadmin"; 
	String SFTPPassword="2Bchanged"; 
	String NasPath="/data/nas/sit/wholesale_amazon/transfers/raise/pending";
	String LocalPath="Test Data/";
	String OrderID="";
	String Prefix="mccolls-AM";
	String tcid="TC001";
	
	
	
	String ordDate="20171214";*/
	
	shipToLocationId="376";
	
	//String matchpattern="mccolls-"+Prefix+"-"+shipToLocationId+"-"+ordDate+"*"+".csv";
	
	String matchpattern="*.csv";
	
	System.out.println(matchpattern);
	
	ChannelSftp sftpchannel=null;
	Channel channel=null;
	
	//String Quantities="10";
	
	//Quantities="4,10";

	//String items="100186050";
	
	//items="100003374,100186050";
	
	String itemList[]=items.split(",");
	
	
	
	
	String itemListQty[]=Quantities.split(",");

itemCount=itemList.length;
	
	
	System.out.println ("Item Count is "+itemCount);
	
	JSch jsch = new JSch();
	Session session = null;
	BufferedReader br = null;

	//ArrayList<ArrayList<Map.Entry<String, String>>> ListofMaps = new ArrayList<ArrayList<Map.Entry<String,String>>>();
		
	ArrayList<HashMap<String, String>> ListofMaps = new ArrayList<HashMap<String,String>>();
	HashMap<String, String> hm = null;
		
	ArrayList<String> HeaderList = null;

		
		try {
			
			session = jsch.getSession(SFTPUserName, SFTPHostName, SFTPPort);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(SFTPPassword);
			session.connect();
			
			channel = session.openChannel("sftp");
			channel.connect();
			
			sftpchannel = (ChannelSftp) channel;

			sftpchannel.cd(NasPath);
			
			System.out.println("The Current path is " + sftpchannel.pwd());
			
			Vector<ChannelSftp.LsEntry> list = sftpchannel.ls(matchpattern);
			
			for(ChannelSftp.LsEntry entry: list) {
				
				String filename = entry.getFilename();
				
			//	System.out.println("The current file is " + filename);
				
			sftpchannel.get(filename, LocalPath + filename);//Copy file from WinSCP to local

			//System.out.println(filename +"  "+ LocalPath + filename);	
				

			
			//Read from local

			String CurrentLine = null;
			int linecount = 0;
			
			String headerRow="";
			
			int matchCount=0;
			
			
			String[] strValuearr = null;
			
			//itemCount=2;
			
			
for(int k=0;k<itemCount;k++)		
				
{
				
	hm = new HashMap<String, String>();

	matchCount=0;
	
	
	
		
	HeaderList = new ArrayList<String>();
	br = new BufferedReader(new FileReader(LocalPath + filename));
	
	linecount=0;
			
	while((CurrentLine = br.readLine()) != null) {


	//	System.out.println(CurrentLine);
		//System.out.println("Item Qty is "+itemListQty[k]);
		
		
		//System.out.println("Item ID is "+itemList[k]);
		
			if(linecount==0){
				
					headerRow=CurrentLine;
					
					//System.out.println("Header row is "+headerRow);
					
				
				}
			
		if(linecount > 0) {
					
if((CurrentLine.contains(itemListQty[k]))&&(CurrentLine.contains(itemList[k]))){
						
	
sftpchannel.get(filename, "Results/MatchedConsolidatedFiles/"+filename);
	
	
//System.out.println("Current item id is "+itemList[k]);
						
matchCount=matchCount+1;
						
utilityFileWriteOP.writeToLog(tcid, "Matched Consolidated File name is ",filename);




//System.out.println("Current match count value is "+matchCount);
						
if(matchCount==1){
							
							
				String[] strarr = headerRow.split(",");
				
				for(int i=0; i<strarr.length; i++){

									
				HeaderList.add(strarr[i]);
				
				
					
				}
				
				
    System.out.println("Header List"+HeaderList);
}			

		
					
else{

		headerRow="";  
				
	}
				//System.out.println("Hiiiii");
				
				strValuearr= CurrentLine.split(",");

                System.out.println("Valid match found!!!");
                
             //   System.out.println("Arrey Length is "+strValuearr.length);
							
							for(int j=0; j<strValuearr.length; j++) {
								
								System.out.println("within for loop");
								
								//FileKeyValues.add(new AbstractMap.SimpleEntry(HeaderList.get(j), strarr[j]));
								System.out.println("About to put "+HeaderList.get(j)+ " and" +strValuearr[j]);
				hm.put(HeaderList.get(j), strValuearr[j]);
								
				//System.out.println(hm);
					}
							
				
							
				ListofMaps.add(hm);
				strValuearr=null;
				
				hm=null;
				
         
			}
	
		}

	  linecount++;
		
	
	}
	

	br.close();		
	
			
	}// End of for			

	}
			

//System.out.println("Header is" +ListofMaps.get(0));
//System.out.println("Header is" +ListofMaps.get(1));


	} 
		
		
catch (Exception e) {
			// TODO: handle exception
	e.printStackTrace();
	System.out.println(e);
	utilityFileWriteOP.writeToLog(tcid, "Error occurred during Temporary Order ID Extraction ", "Due to :"+e);

}
		
	
finally{
			
//br.close();		
sftpchannel.exit();			
session.disconnect();
	
}
		
return ListofMaps;

}




public static ArrayList<HashMap<String, String>> OrderParameters(
		String Exp_customerId,String Exp_OrderId,	String Exp_MessageType,String Exp_ShipToLocationId,
		
		String Exp_ShipToDeliverAt,String Exp_OrdRefCode,
		String Exp_quantityOrdered,String Exp_itemId, List<String> ItemDetailsList,String tcid

) throws IOException {

	int itemCount=0;

	String itemList[]=Exp_itemId.split(",");

	String itemListQty[]=Exp_quantityOrdered.split(",");

     itemCount=itemList.length;
	
	
	System.out.println ("Item Count is "+itemCount);
	
	ArrayList<HashMap<String, String>> ListofMaps = new ArrayList<HashMap<String,String>>();
	
	
	HashMap<String, String> hm = null;
		

		
try {
			

			
for(int k=0;k<itemCount;k++)		
				
{
				
	hm = new HashMap<String, String>();
	
	hm.put("Exp_customerId", Exp_customerId);
	hm.put("Exp_OrderId", Exp_OrderId);
	hm.put("Exp_MessageType", Exp_MessageType);
	hm.put("Exp_ShipToLocationId", Exp_ShipToLocationId);
	hm.put("Exp_ShipToDeliverAt", Exp_ShipToDeliverAt);
	hm.put("Exp_OrdRefCode", Exp_OrdRefCode);
	hm.put("Exp_quantityOrdered", itemListQty[k]);
	hm.put("Exp_itemId", itemList[k]);
	
	
	System.out.println("Item "+k+"json array is "+ItemDetailsList);

					
			
	ListofMaps.add(hm);
	
	
	System.out.println("Map Added"+hm);
				
				
	hm=null;
				
         
}
		



} 
		
		
catch (Exception e) {
			// TODO: handle exception
	e.printStackTrace();
	System.out.println(e);
	//utilityFileWriteOP.writeToLog(tcid, "Error occurred during Temporary Order ID Extraction ", "Due to :"+e);

}
		
	
finally{
			
	return ListofMaps;
	
}
		


}



public static ArrayList<HashMap<String, String>> OrderParametersExcel(
		String Exp_customerId,String Exp_OrderId,	String Exp_MessageType,String Exp_ShipToLocationId,
		
		String Exp_ShipToDeliverAt,String Exp_OrdRefCode,
		String Exp_quantityOrdered,String Exp_itemId, List<String> ItemDetailsList,String TempOrderID,String tcid

) throws IOException {

	int itemCount=0;

	String itemList[]=Exp_itemId.split(",");

	String itemListQty[]=Exp_quantityOrdered.split(",");

     itemCount=itemList.length;
	
	
	System.out.println ("Item Count is "+itemCount);
	
	ArrayList<HashMap<String, String>> ListofMaps = new ArrayList<HashMap<String,String>>();
	
	
	HashMap<String, String> hm = null;
		

		
try {
			

			
for(int k=0;k<itemCount;k++)		
				
{
				
	hm = new HashMap<String, String>();
	
	hm.put("customerId", Exp_customerId);
	
	hm.put("orderId", TempOrderID);
	
	
	
	//hm.put("Exp_OrderId", Exp_OrderId);
	
	
	hm.put("messageType", Exp_MessageType);
	
	
	hm.put("shipToLocationId", Exp_ShipToLocationId);
	
	
	hm.put("shipToDeliverAt", Exp_ShipToDeliverAt);
	
	
	
	
	
	hm.put("orderReferenceCode", Exp_OrdRefCode);
	
	

	
	
	
	
	String Exp_ItemDetailsJson=ItemDetailsList.get(k);
	 
	 
	 
	 JsonElement jsonelement = new JsonParser().parse(Exp_ItemDetailsJson);
	 JsonObject jsonobject = jsonelement.getAsJsonObject();

	 String Exp_quantityType = jsonobject.get("quantityType").toString().replaceAll("^\"|\"$", "");
	 
	 
	 
	 
	 
	 
	 String Exp_itemIdJson= jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "");

	 String Exp_itemDescription= jsonobject.get("itemDescription").toString().replaceAll("^\"|\"$", "");
	 
	// String Exp_itemCaseSize= jsonobject.get("itemCaseSize").toString().replaceAll("^\"|\"$", "");
	 
	 String Exp_quantityOrderedJson= jsonobject.get("quantityOrdered").toString().replaceAll("^\"|\"$", "");
	 
	 
	 
	
	
		hm.put("quantityOrdered", Exp_quantityOrderedJson);
		
		
		hm.put("itemId",Exp_itemIdJson);
		
		
		hm.put("itemDescription", Exp_itemDescription);
	
		hm.put("quantityType", Exp_quantityType);
	

	
	//System.out.println("Item "+k+"json array is "+ItemDetailsList);

					
			
	ListofMaps.add(hm);
	
	
	System.out.println("Map Added"+hm);
				
				
	hm=null;
				
         
}
		



} 
		
		
catch (Exception e) {
			// TODO: handle exception
	e.printStackTrace();
	System.out.println(e);
	//utilityFileWriteOP.writeToLog(tcid, "Error occurred during Temporary Order ID Extraction ", "Due to :"+e);

}
		
	
finally{
			
	return ListofMaps;
	
}
		


}








public static ArrayList<HashMap<String, String>> NASContent(String SFTPHostName,
		int SFTPPort,String SFTPUserName,String SFTPPassword,String NasPath,String LocalPath,
		
		String OrderID,	String Prefix,String shipToLocationId,String ordDate,String Quantities,
		String items,String tcid
             ) throws IOException {
	
	
	
		
	int itemCount=0;
	/*String SFTPHostName="tgmus03x"; 
	int SFTPPort=22; 
	String SFTPUserName="mwadmin"; 
	String SFTPPassword="2Bchanged"; 
	String NasPath="/data/nas/sit/wholesale_amazon/transfers/raise/pending";
	String LocalPath="Test Data/";
	String OrderID="";
	String Prefix="mccolls-AM";
	String tcid="TC001";
	
	
	
	String ordDate="20171214";*/
	
	shipToLocationId="376";
	
	//String matchpattern="mccolls-"+Prefix+"-"+shipToLocationId+"-"+ordDate+"*"+".csv";
	
	String matchpattern="*.csv";
	
	System.out.println(matchpattern);
	
	ChannelSftp sftpchannel=null;
	Channel channel=null;
	
	//String Quantities="10";
	
	//Quantities="4,10";

	//String items="100186050";
	
	//items="100003374,100186050";
	
String itemList[]=items.split(",");
	
	
	
	
String itemListQty[]=Quantities.split(",");

itemCount=itemList.length;
	
	
	System.out.println ("Item Count is "+itemCount);
	
	JSch jsch = new JSch();
	Session session = null;
	BufferedReader br = null;

	//ArrayList<ArrayList<Map.Entry<String, String>>> ListofMaps = new ArrayList<ArrayList<Map.Entry<String,String>>>();
		
	ArrayList<HashMap<String, String>> ListofMaps = new ArrayList<HashMap<String,String>>();
	HashMap<String, String> hm = null;
		
	ArrayList<String> HeaderList = null;

		
		try {
			
			session = jsch.getSession(SFTPUserName, SFTPHostName, SFTPPort);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(SFTPPassword);
			session.connect();
			
			channel = session.openChannel("sftp");
			channel.connect();
			
			sftpchannel = (ChannelSftp) channel;

			sftpchannel.cd(NasPath);
			
			System.out.println("The Current path is " + sftpchannel.pwd());
			
			Vector<ChannelSftp.LsEntry> list = sftpchannel.ls(matchpattern);
			
			for(ChannelSftp.LsEntry entry: list) {
				
				String filename = entry.getFilename();
				
				System.out.println("The current file is " + filename);
				
			sftpchannel.get(filename, LocalPath + filename);//Copy file from WinSCP to local

			//System.out.println(filename +"  "+ LocalPath + filename);	
				

			
			//Read from local

			String CurrentLine = null;
			int linecount = 0;
			
			String headerRow="";
			
			int matchCount=0;
			
			
			String[] strValuearr = null;
			
			//itemCount=2;
			
			
for(int k=0;k<itemCount;k++)		
				
{
				
	hm = new HashMap<String, String>();

	matchCount=0;
	
	
	
		
	HeaderList = new ArrayList<String>();
	br = new BufferedReader(new FileReader(LocalPath + filename));
	
	linecount=0;
			
	while((CurrentLine = br.readLine()) != null) {


	//	System.out.println(CurrentLine);
		//System.out.println("Item Qty is "+itemListQty[k]);
		
		
		//System.out.println("Item ID is "+itemList[k]);
		
			if(linecount==0){
				
					headerRow=CurrentLine;
					
					//System.out.println("Header row is "+headerRow);
					
				
				}

			
			if(linecount > 0) {
					
if((CurrentLine.contains(itemListQty[k]))&&(CurrentLine.contains(itemList[k]))){
						
	
sftpchannel.get(filename, "Results/MatchedConsolidatedFiles/"+filename);
	
	
//System.out.println("Current item id is "+itemList[k]);
						
matchCount=matchCount+1;
						
utilityFileWriteOP.writeToLog(tcid, "Matched Consolidated File name is ",filename);




System.out.println("Matched Consolidated File name is "+filename);
						
if(matchCount==1){
							
							
				String[] strarr = headerRow.split(",");
				
				for(int i=0; i<strarr.length; i++){

									
				HeaderList.add(strarr[i]);
				
				
					
				}
				
				
			//System.out.println("Header List"+HeaderList);
}			

		
					
else{

		headerRow="";  
				
	}
				//System.out.println("Hiiiii");
				
				strValuearr= CurrentLine.split(",");

               // System.out.println("Valid match found!!!");
                
             //   System.out.println("Arrey Length is "+strValuearr.length);
							
							for(int j=0; j<strValuearr.length; j++) {
								
								//System.out.println("within for loop");
								
								//FileKeyValues.add(new AbstractMap.SimpleEntry(HeaderList.get(j), strarr[j]));
								//System.out.println("About to put "+HeaderList.get(j)+ " and" +strValuearr[j]);
				hm.put(HeaderList.get(j), strValuearr[j]);
								
				//System.out.println(hm);
					}
							
				
							
				ListofMaps.add(hm);
				strValuearr=null;
				
				hm=null;
				
         
			}
	
		}

	  linecount++;
		
	
	}
	

	br.close();		
	
			
	}// End of for			

	}
			

//System.out.println("Header is" +ListofMaps.get(0));
//System.out.println("Header is" +ListofMaps.get(1));


	} 
		
		
catch (Exception e) {
			// TODO: handle exception
	e.printStackTrace();
	System.out.println(e);
	utilityFileWriteOP.writeToLog(tcid, "Error occurred during Temporary Order ID Extraction ", "Due to :"+e);

}
		
	
finally{
			
//br.close();		
sftpchannel.exit();			
session.disconnect();
	
}
		
return ListofMaps;

}



public static ArrayList<HashMap<String, String>> NASContent_Retrive(String SFTPHostName,
		int SFTPPort,String SFTPUserName,String SFTPPassword,String NasPath,String LocalPath,
		
			String OrderRefCode,String shipToLocationId, List<String> l,String tcid
             
		)     throws IOException {

	int Exp_itemCount=l.size();

	//shipToLocationId="376";
	
	//String matchpattern="mccolls-"+Prefix+"-"+shipToLocationId+"-"+ordDate+"*"+".csv";
	
	//String matchpattern="*.csv";
	
	String matchpattern="mccolls-"+OrderRefCode+"-"+shipToLocationId+"*"+".csv";

	System.out.println(matchpattern);
	
	ChannelSftp sftpchannel=null;
	Channel channel=null;

	
	JSch jsch = new JSch();
	Session session = null;
	BufferedReader br = null;

	ArrayList<HashMap<String, String>> ListofMaps = new ArrayList<HashMap<String,String>>();
	HashMap<String, String> hm = null;
		
	ArrayList<String> HeaderList = null;

		
		try {
			
			session = jsch.getSession(SFTPUserName, SFTPHostName, SFTPPort);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(SFTPPassword);
			session.connect();
			
			channel = session.openChannel("sftp");
			channel.connect();
			
			sftpchannel = (ChannelSftp) channel;

			sftpchannel.cd(NasPath);
			
			System.out.println("The Current path is " + sftpchannel.pwd());
			
			Vector<ChannelSftp.LsEntry> list = sftpchannel.ls(matchpattern);
			
			
		
			
	for(ChannelSftp.LsEntry entry: list) {

		String filename = entry.getFilename();
				
		System.out.println("The current file is " + filename);
				
			sftpchannel.get(filename, LocalPath + filename);//Copy file from WinSCP to local

			//System.out.println(filename +"  "+ LocalPath + filename);	
				
			
			
			//Read from local

			String CurrentLine = null;
			int linecount = 0;

			
			
			
			String[] strValuearr = null;


	
	HeaderList = new ArrayList<String>();
	br = new BufferedReader(new FileReader(LocalPath + filename));
	
	linecount=0;
			
	while((CurrentLine = br.readLine()) != null) {

		
			if(linecount==0){
				
	
		     }

			
if(linecount > 0) {
					
	
sftpchannel.get(filename, "Results/MatchedConsolidatedFiles/"+filename);


hm = new HashMap<String, String>();

				strValuearr= CurrentLine.split(",");
		/*
				hm.put("customerId", strValuearr[0]);
				hm.put("orderId", strValuearr[1]);
				hm.put("itemId", strValuearr[21]);
		*/		
				
		    	hm.put("customerId", strValuearr[0]);

		    	hm.put("orderId", strValuearr[1]);

		    	hm.put("orderReferenceCode", strValuearr[4]);

		    	hm.put("messageId", strValuearr[8]);

		    	hm.put("shipToLocationId", strValuearr[11]);

		    	hm.put("itemId", strValuearr[21]);

		    	hm.put("quantityOrdered", strValuearr[27]);
		    	
		    	
				
				
				
				
				
				
				
				
				
				
	            ListofMaps.add(hm);
	
	

	  }

	  
linecount++;
		
	
}
	
	
	if((linecount-1)>Exp_itemCount){
	
	
		  utilityFileWriteOP.writeToLog(tcid, "Consolidated file in NAS contains more Item then expected ","");
		
	
	}
	
	
	
	
	
	br.close();		
	
	
		}


	} 
		
		
catch (Exception e) {
			// TODO: handle exception
	e.printStackTrace();
	System.out.println(e);
	utilityFileWriteOP.writeToLog(tcid, "Error occurred during Temporary Order ID Extraction ", "Due to :"+e);

}

finally{
			
//br.close();		
sftpchannel.exit();			
session.disconnect();
	
}
		
return ListofMaps;

}





public static ArrayList<HashMap<String, String>> NASContent_RetriveNegativeSc(String SFTPHostName,
		int SFTPPort,String SFTPUserName,String SFTPPassword,String NasPath,String ShiptoDeliverDt,String LocalPath,
		
			String OrderRefCode,String shipToLocationId, List<String> l,String tcid,String ItemTypeList,String TotalItemList
             
		)     throws IOException {

	int Exp_itemCount=l.size();
	
	
	String DeliveryDt=ShiptoDeliverDt.replaceAll("-","");
	
	
	String [] ItemsAll_Type=ItemTypeList.split(",");
	
	String [] ItemsAll=TotalItemList.split(",");

	String[] ItemsListValid = new String[50];
	
	
	for(int k=0;k<ItemsAll_Type.length;k++){
	
	
	if(ItemsAll_Type[k].contentEquals("P")){
		
		ItemsListValid[k]="P";
			
	       }
	}
	
	
	int NoOfRecordsInNAS=0;
	
	int NoOfValidItems=ItemsAll_Type.length;

	
//String matchpattern="mccolls-"+OrderRefCode+"-"+shipToLocationId+"-"+"20190825"+"-"+"*"+".csv";
	
	String matchpattern="mccolls-"+OrderRefCode+"-"+shipToLocationId+"-"+DeliveryDt+"-"+"*"+".csv";	
	
	System.out.println("Ship to location ID is "+shipToLocationId);

	System.out.println(matchpattern);
	
	ChannelSftp sftpchannel=null;
	Channel channel=null;
	
	JSch jsch = new JSch();
	Session session = null;
	BufferedReader br = null;

	ArrayList<HashMap<String, String>> ListofMaps = new ArrayList<HashMap<String,String>>();
	HashMap<String, String> hm = null;
		
	ArrayList<String> HeaderList = null;

		
		try {
			
			session = jsch.getSession(SFTPUserName, SFTPHostName, SFTPPort);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(SFTPPassword);
			session.connect();
			
			channel = session.openChannel("sftp");
			channel.connect();
			
			sftpchannel = (ChannelSftp) channel;

			sftpchannel.cd(NasPath);
			
			System.out.println("The Current path is " + sftpchannel.pwd());
			
			Vector<ChannelSftp.LsEntry> list = sftpchannel.ls(matchpattern);
			
			
		
			
	for(ChannelSftp.LsEntry entry: list) {

		String filename = entry.getFilename();
				
		System.out.println("The current file is " + filename);
				
			sftpchannel.get(filename, LocalPath + filename);//Copy file from WinSCP to local

			//System.out.println(filename +"  "+ LocalPath + filename);	
				
			
			
			//Read from local

			String CurrentLine = null;
			int linecount = 0;

			
			
			
			String[] strValuearr = null;


	
	HeaderList = new ArrayList<String>();
	br = new BufferedReader(new FileReader(LocalPath + filename));
	
	linecount=0;
			
	while((CurrentLine = br.readLine()) != null) {

		
			if(linecount==0){
				
	
		     }

			
if(linecount > 0) {
	
	
	
	File theDir = new File("Results/MatchedConsolidatedFiles");

	// if the directory does not exist, create it
if (!theDir.exists()) {
	    System.out.println("creating directory: " + theDir.getName());
	    
	    
	    boolean result = false;

	    try{
	        theDir.mkdir();
	        result = true;
	    } 
	    catch(SecurityException se){
	        //handle it
	    }        
	    if(result) {    
	       
	    System.out.println("MatchedConsolidatedFiles  DIR created");  
	   
	    }
	}	
	
	
					
	
sftpchannel.get(filename, "Results/MatchedConsolidatedFiles/"+filename);


hm = new HashMap<String, String>();

				strValuearr= CurrentLine.split(",");
		/*
				hm.put("customerId", strValuearr[0]);
				hm.put("orderId", strValuearr[1]);
				hm.put("itemId", strValuearr[21]);
		*/		
				
		    	hm.put("customerId", strValuearr[0]);

		    	hm.put("orderId", strValuearr[1]);

		    	hm.put("orderReferenceCode", strValuearr[4]);

		    	hm.put("messageId", strValuearr[8]);

		    	hm.put("shipToLocationId", strValuearr[11]);

		    	hm.put("itemId", strValuearr[21]);

		    	hm.put("quantityOrdered", strValuearr[27]);
		    	
		    	hm.put("quantityType", strValuearr[26]);
		    	
		    	hm.put("itemDescription", strValuearr[25]);
		    	
		    	
		    	hm.put("itemAlternateId", strValuearr[24]);
		    	
		    	hm.put("shipToDeliverAt", strValuearr[19]);
		    	
		    	hm.put("createdAt", strValuearr[28]);
		    	
		    	
		    	
		    	ListofMaps.add(hm);
	
	            
	            NoOfRecordsInNAS=NoOfRecordsInNAS+1;
	            
	

	  }

	  
linecount++;
		
	
}
	
	
	if((linecount-1)>Exp_itemCount){
	
	
		  utilityFileWriteOP.writeToLog(tcid, "Consolidated file in NAS contains more Item then expected ","");
		
	
	}


	br.close();		
	
	
		}


	} 
		
		
catch (Exception e) {
			// TODO: handle exception
	e.printStackTrace();
	System.out.println(e);
	utilityFileWriteOP.writeToLog(tcid, "Error occurred during Temporary Order ID Extraction ", "Due to :"+e);

}

finally{
			
//br.close();		
sftpchannel.exit();			
session.disconnect();


return ListofMaps;
	
     }
		

	}




public static ArrayList<HashMap<String, String>> NASContent_Eaches(String SFTPHostName,
		int SFTPPort,String SFTPUserName,String SFTPPassword,String NasPath,String LocalPath,
		
		String OrderID,	String OrderRefCode,String shipToLocationId,String ordDate,String Quantities,
		String items,String tcid
             ) throws IOException {
	
	
	
		
	int itemCount=0;
	/*String SFTPHostName="tgmus03x"; 
	int SFTPPort=22; 
	String SFTPUserName="mwadmin"; 
	String SFTPPassword="2Bchanged"; 
	String NasPath="/data/nas/sit/wholesale_amazon/transfers/raise/pending";
	String LocalPath="Test Data/";
	String OrderID="";
	String Prefix="mccolls-AM";
	String tcid="TC001";
	
	
	
	String ordDate="20171214";*/
	
	shipToLocationId="376";
	
	//String matchpattern="mccolls-"+Prefix+"-"+shipToLocationId+"-"+ordDate+"*"+".csv";
	
	//String matchpattern="*.csv";
	
	String matchpattern="mccolls-"+OrderRefCode+"*"+".csv";
	
	
	
	System.out.println(matchpattern);
	
	ChannelSftp sftpchannel=null;
	Channel channel=null;
	
	//String Quantities="10";
	
	//Quantities="4,10";

	//String items="100186050";
	
	//items="100003374,100186050";
	
String itemList[]=items.split(",");
	
	
	
	
String itemListQty[]=Quantities.split(",");

itemCount=itemList.length;
	
	
	System.out.println ("Item Count is "+itemCount);
	
	JSch jsch = new JSch();
	Session session = null;
	BufferedReader br = null;

	//ArrayList<ArrayList<Map.Entry<String, String>>> ListofMaps = new ArrayList<ArrayList<Map.Entry<String,String>>>();
		
	ArrayList<HashMap<String, String>> ListofMaps = new ArrayList<HashMap<String,String>>();
	HashMap<String, String> hm = null;
		
	ArrayList<String> HeaderList = null;

		
		try {
			
			session = jsch.getSession(SFTPUserName, SFTPHostName, SFTPPort);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(SFTPPassword);
			session.connect();
			
			channel = session.openChannel("sftp");
			channel.connect();
			
			sftpchannel = (ChannelSftp) channel;

			sftpchannel.cd(NasPath);
			
			System.out.println("The Current path is " + sftpchannel.pwd());
			
			Vector<ChannelSftp.LsEntry> list = sftpchannel.ls(matchpattern);
			
			
			int noOfConsolidatedFile=0;
			
			for(ChannelSftp.LsEntry entry: list) {
				
				
				
				
				
				String filename = entry.getFilename();
				
				System.out.println("The current file is " + filename);
				
			sftpchannel.get(filename, LocalPath + filename);//Copy file from WinSCP to local

			//System.out.println(filename +"  "+ LocalPath + filename);	
				
			noOfConsolidatedFile=noOfConsolidatedFile+1;
			
			//Read from local

			String CurrentLine = null;
			int linecount = 0;
			
			String headerRow="";
			
			int matchCount=0;
			
			
			String[] strValuearr = null;
			
			//itemCount=2;
			
			
for(int k=0;k<itemCount;k++)		
				
{
				
	hm = new HashMap<String, String>();

	matchCount=0;
	
	
	
		
	HeaderList = new ArrayList<String>();
	br = new BufferedReader(new FileReader(LocalPath + filename));
	
	linecount=0;
			
	while((CurrentLine = br.readLine()) != null) {


	//	System.out.println(CurrentLine);
		//System.out.println("Item Qty is "+itemListQty[k]);
		
		
		//System.out.println("Item ID is "+itemList[k]);
		
			if(linecount==0){
				
					headerRow=CurrentLine;
					
					//System.out.println("Header row is "+headerRow);
					
				
				}
			
			
			
			

			
if(linecount > 0) {
	
	
//	if((CurrentLine.contains(itemListQty[k]))&&(CurrentLine.contains(itemList[k]))){
					
if((CurrentLine.contains(itemList[k]))){
						
	
sftpchannel.get(filename, "Results/MatchedConsolidatedFiles/"+filename);
	
	
//System.out.println("Current item id is "+itemList[k]);
						
matchCount=matchCount+1;
						
utilityFileWriteOP.writeToLog(tcid, "Matched Consolidated File name is ",filename);




System.out.println("Matched Consolidated File name is "+filename);
						
if(matchCount==1){
							
							
				String[] strarr = headerRow.split(",");
				
				for(int i=0; i<strarr.length; i++){

									
				HeaderList.add(strarr[i]);
				
				
					
				}
				
				
			//System.out.println("Header List"+HeaderList);
}			

		
					
else{

		headerRow="";  
				
	}
				//System.out.println("Hiiiii");
				
				strValuearr= CurrentLine.split(",");

               // System.out.println("Valid match found!!!");
                
             //   System.out.println("Arrey Length is "+strValuearr.length);
							
							for(int j=0; j<strValuearr.length; j++) {
								
								//System.out.println("within for loop");
								
								//FileKeyValues.add(new AbstractMap.SimpleEntry(HeaderList.get(j), strarr[j]));
								//System.out.println("About to put "+HeaderList.get(j)+ " and" +strValuearr[j]);
				hm.put(HeaderList.get(j), strValuearr[j]);
								
				//System.out.println(hm);
					}
							
				
							
				ListofMaps.add(hm);
				strValuearr=null;
				
				hm=null;
				
         
			}
	
		}

	  linecount++;
		
	
	}
	

	br.close();		
	
			
	}// End of for			

	
			
if(noOfConsolidatedFile>1){	
	
	
	utilityFileWriteOP.writeToLog(tcid, "More consolidated file exists in NAS with same Order reference code", "Pre Condition Not met for execution -FAIL");
	
		
}		
			}
			

//System.out.println("Header is" +ListofMaps.get(0));
//System.out.println("Header is" +ListofMaps.get(1));


	} 
		
		
catch (Exception e) {
			// TODO: handle exception
	e.printStackTrace();
	System.out.println(e);
	utilityFileWriteOP.writeToLog(tcid, "Error occurred during Temporary Order ID Extraction ", "Due to :"+e);

}
		
	
finally{
			
//br.close();		
sftpchannel.exit();			
session.disconnect();
	
}
		
return ListofMaps;

}



































public static long LastModifiedFileTime(String SFTPHostName, int SFTPPort, String SFTPUserName, String SFTPPassword, String NasPath, String TemporaryFilePath, String tcid) throws IOException {
	
	JSch jsch = new JSch();
	Session session = null;
	//BufferedReader br = null;

	long lastmodifiedfiletime = 0;
	//String Result = null;
	//int flag = 0;
	
	try {
		
		session = jsch.getSession(SFTPUserName, SFTPHostName, SFTPPort);
		session.setConfig("StrictHostKeyChecking", "no");
		session.setPassword(SFTPPassword);
		session.connect();
		
		Channel channel = session.openChannel("sftp");
		channel.connect();
		
		ChannelSftp sftpchannel = (ChannelSftp) channel;
				
		sftpchannel.cd(NasPath);
		
		System.out.println("The Current path is " + sftpchannel.pwd());
		
		Vector<ChannelSftp.LsEntry> list = sftpchannel.ls("*.csv");
		
		for(ChannelSftp.LsEntry entry: list) {
			
			String filename = entry.getFilename();
			
			System.out.println("The current file is " + filename);
			
			
			
			sftpchannel.get(filename, TemporaryFilePath + filename);//Copy file from WinSCP to local
			
			Date currentfilelastmodifieddate = new Date(new File(TemporaryFilePath + filename).lastModified());

			long currentfilelastmodifiedmilliseconds = currentfilelastmodifieddate.getTime();

			if(currentfilelastmodifiedmilliseconds > lastmodifiedfiletime) {

				lastmodifiedfiletime = currentfilelastmodifiedmilliseconds;

			}
			
		
			
		}
		
		FileUtils.cleanDirectory(new File(TemporaryFilePath)); // Delete all downloaded files from local directory
		
		//br.close();
		sftpchannel.exit();
		
		session.disconnect();
		
	
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		System.out.println(e);
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during Temporary Order ID Extraction ", "Due to :"+e);
		return lastmodifiedfiletime;
	}
	
	
	

	
	return lastmodifiedfiletime;
}


	public static ArrayList<String> OrderIDListExtract(String DriverSheetPath, String SheetName) {
		
		ArrayList<String> OrderIDList = new ArrayList<String>();
		
		String OrderID = null;
		
		int r = 0;
		
		try {
			
			int rows = 0;
			
			Workbook wrk1 = Workbook.getWorkbook(new File(DriverSheetPath));
			
			Sheet sheet1 = wrk1.getSheet(SheetName);
			
			rows = sheet1.getRows();
			
			for(r=1; r<rows; r++) {
				
				OrderID = sheet1.getCell(16, r).getContents().trim();
				
				OrderIDList.add(OrderID);
			}
			
			
			return OrderIDList;
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			
			OrderIDList = null;
			return OrderIDList;
		}
		
		
	}
	
	public static boolean mccollsvalidateordersInRDS(String orderid,String[] columnname, String[] itemid, ArrayList list, String tcid) throws JSchException{
		
		 Channel channel=null;
		 Session session=null;
		 
		 boolean res=true;
		 
		 ArrayList<Map> itemlist=list;
	     
	     String QResult="";

	        try{      
	              
	              JSch jsch = new JSch();
	              
	              java.util.Properties config = new java.util.Properties(); 
	              
	           //   System.out.println("0");

	              //String command = "psql --host=generic01-nonprod-nonprod-rdsgeneric-del.ca107skwgezh.eu-west-1.rds.amazonaws.com api_stock_v1_db_sit --user tcs_testing;";
	              String command = "psql --host=rds-euw1-sit-wholesale-integration-001.ca107skwgezh.eu-west-1.rds.amazonaws.com npwholesaleintsit --user npwholesaleintsit_user";
	              
	              
	            session = jsch.getSession("mwadmin","10.244.39.83",22);
	              
	             config.put("StrictHostKeyChecking", "no");
	              
	              session.setConfig(config);
	              
	         //    System.out.println("1");
	              
	          session.setPassword("2Bchanged");
	          
	          session.connect();
	              
	           //   System.out.println("Connection success");

	              channel = session.openChannel("exec");
	              ((ChannelExec)channel).setCommand(command);

	              channel.setInputStream(null);
	              ((ChannelExec)channel).setErrStream(System.err);
	              InputStream in = channel.getInputStream();
	            ((ChannelExec)channel).setPty(false);
	              
	              OutputStream out=channel.getOutputStream();
	              channel.connect();
	              
	              Thread.sleep(1000);
	              
	        out.write(("user124676r4"+"\n").getBytes());
	        out.flush();
	        
	        
	        for(int item=0;item<itemlist.size();item++)
	        {
	        for(int col=0;col<columnname.length;col++)
	        {
	        Thread.sleep(1000);
	        QResult="";
	        out.flush();
	        
	        if(columnname[col].equals("orderId"))
	        {
	        	columnname[col]="temporaryorderId";
	        }
	        
	     //out.write(("select value,min from transaction_hist where min ='"+item_no+"' and location_id='"+location+"' order by created desc limit 1;"+"\n").getBytes());
	     out.write(("select distinct "+columnname[col]+" from orderconsolidations where orderid ='"+orderid+"' and itemid ='"+itemid[item]+"';\n").getBytes());
	     
	        out.flush();
	        
	        Thread.sleep(1000);
	        
	        
	        
	        Thread.sleep(5000);

	     // channel.setOutputStream(System.out);

	      //  System.out.println("2");
	        
	        byte[] tmp = new byte[2048];
	           
	        int i=0;  
	              
	        
	          while (in.available() > 0) {
	                	
	                	{
	                  
	                i = in.read(tmp, 0, 1024);
	                   
	                    System.out.println("i "+i);
	                  //  System.out.println(i);
	                    
	                    if (i < 0) {
	                        break;
	 
	                    }
	                  
	           //   System.out.print(new String(tmp, 0, i));
	                    
	                }  
	                	
	                	
	               
	               }
	          
	        QResult=new String(tmp, 0, i); 
	        String[] rest= QResult.split(columnname[col]);
			 String justfinalres=rest[rest.length-1].replaceAll("[-+|]", "");
			 //System.out.println("QResult is "+QResult+" is this!!!");
			 //System.out.println("justfinalres "+justfinalres);
	    	 String[] rest2=justfinalres.split("[\\r\\n]+");
	    	 /*
	    	 for(int restcount=0;restcount<rest2.length;restcount++)
	    	 {
	    	 System.out.println("rest2["+restcount+"] "+rest2[restcount]);
	    	 }*/
	    	 /*String finalres="";
	    	 if(col==0 && rest2.length>5)
	    	 {
	    		 finalres=rest2[5].trim();
	    	 }
	    	 else
	    	 {
	    	 finalres=rest2[2].trim();
	    	 }*/
	    	 String finalres="";
	    	 for(int restcount=0;restcount<rest2.length;restcount++)
	    	 {
	    	 System.out.println("rest2["+restcount+"] "+rest2[restcount]);
	    	 if(rest2[restcount].contains("("))
	    	 {
	    		 if(!rest2[restcount-1].equals(columnname))
	    		 {
	    		 finalres=rest2[restcount-1].trim();
	    		 
	    		 break;
	    		 }
	    	 }
	    	 
	    	 }
	    	 System.out.println("finalres "+finalres);
	    	 
	    	 //System.out.println(columnname[col]);
	    	 
	    	 //System.out.println(itemid[item]);
	    	//System.out.println(itemlist.get(item).toString());
	    	//System.out.println(columnname[col]);
	    	 String nasres="";
	    	 
	    	 if(columnname[col].equalsIgnoreCase("temporaryorderid"))
	    	 {
	    		 nasres=itemlist.get(item).get("orderId").toString().replaceAll("[-+|]", "");
		    	 
	    	 }
	    	 else
	    	 {
	    		 nasres=itemlist.get(item).get(columnname[col]).toString().replaceAll("[-+|]", "");
		    	  
	    	 }
	    	 
	    	 if(nasres.equalsIgnoreCase(finalres))
	    	 {
	    		 Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname[col]+" from NAS file "+nasres, "Matched with the value found in RDS DB "+finalres);
	    	 }
	    	 else
	    	 {		
	    		 if(finalres.equals("(0 rows)")||finalres.equals(""))
	    		 {
	    			 
	    			 Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname[col]+" for Order "+orderid+" and item "+itemid[item]+" from NAS file "+nasres, "Did not match because NO value was found in RDS DB ");
		    		 
	    		 }
	    		 else
	    		 {
	    		 Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname[col]+" for Order "+orderid+" and item "+itemid[item]+" from NAS file "+nasres, "Did not match with the value found in RDS DB "+finalres);
	    		 }
	    		 res=false;
	    	 }
	    	 
	    	 
	        }
	        }
	        
	        out.flush();
	        out.write(("\\q"+"\n").getBytes());
	        
	        out.flush();
	        
	        Thread.sleep(5000);
	      //  String [] res=   QResult.split(" ");

	/*        System.out.println("4");
	   for(int j=0;j<res.length;j++){
	
	   if(McOlls_I5a_RDS_DB.isInteger(res[j])){
		   
	    	System.out.println(res[j].trim());
	    	adj_val_rds=res[j].trim();
	    	
	     }  
	   

	   }*/
	   
	   
	// System.out.println( session.isConnected());

	  channel.disconnect();

	  session.disconnect();
	  
	  Thread.sleep(5000);
	  
	  out.close();
	  
	  in.close();
	  
	//System.out.println(adj_val_rds);
	  
	 

 }
	              

	        
catch(Exception e){
e.printStackTrace();
 System.out.println(e);

	        } 
	
	
	finally{        
	        
		
		
		//System.out.println(session.isConnected());

		    if (channel != null) {
		      session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
		       // System.out.println(channel.isConnected());
		    }
		
	      return res;
	}

	}
	        

	public static boolean mccollsvalidateordersInRDS(String orderid,String columnname, String[] itemid, String columnvalue, String tcid) throws JSchException{
		
		 Channel channel=null;
		 Session session=null;
		 
		 boolean res=true;
		 
		 
	     
	     String QResult="";

	        try{      
	              
	              JSch jsch = new JSch();
	              
	              java.util.Properties config = new java.util.Properties(); 
	              
	           //   System.out.println("0");

	              //String command = "psql --host=generic01-nonprod-nonprod-rdsgeneric-del.ca107skwgezh.eu-west-1.rds.amazonaws.com api_stock_v1_db_sit --user tcs_testing;";
	              String command = "psql --host=rds-euw1-sit-wholesale-integration-001.ca107skwgezh.eu-west-1.rds.amazonaws.com npwholesaleintsit --user npwholesaleintsit_user";
	              
	              
	            session = jsch.getSession("mwadmin","10.244.39.83",22);
	              
	             config.put("StrictHostKeyChecking", "no");
	              
	              session.setConfig(config);
	              
	         //    System.out.println("1");
	              
	          session.setPassword("2Bchanged");
	          
	          session.connect();
	              
	           //   System.out.println("Connection success");

	              channel = session.openChannel("exec");
	              ((ChannelExec)channel).setCommand(command);

	              channel.setInputStream(null);
	              ((ChannelExec)channel).setErrStream(System.err);
	              InputStream in = channel.getInputStream();
	            ((ChannelExec)channel).setPty(false);
	              
	              OutputStream out=channel.getOutputStream();
	              channel.connect();
	              
	              Thread.sleep(1000);
	              
	        out.write(("user124676r4"+"\n").getBytes());
	        out.flush();
	        
	        
	        for(int item=0;item<itemid.length;item++)
	        {
	        
	        Thread.sleep(1000);
	        QResult="";
	        out.flush();
	        
	     //out.write(("select value,min from transaction_hist where min ='"+item_no+"' and location_id='"+location+"' order by created desc limit 1;"+"\n").getBytes());
	     out.write(("select distinct "+columnname+" from orderconsolidations where orderid ='"+orderid+"' and itemid ='"+itemid[item]+"';\n").getBytes());
	     
	        out.flush();
	        
	        Thread.sleep(1000);
	        
	        
	        
	        Thread.sleep(5000);

	     // channel.setOutputStream(System.out);

	      //  System.out.println("2");
	        
	        byte[] tmp = new byte[2048];
	           
	        int i=0;  
	              
	        
	          while (in.available() > 0) {
	                	
	                	{
	                  
	                i = in.read(tmp, 0, 1024);
	                   
	                    System.out.println("i "+i);
	                  //  System.out.println(i);
	                    
	                    if (i < 0) {
	                        break;
	 
	                    }
	                  
	           //   System.out.print(new String(tmp, 0, i));
	                    
	                }  
	                	
	                	
	               
	               }
	          
	        QResult=new String(tmp, 0, i); 
	        String[] rest= QResult.split(columnname);
			 String justfinalres=rest[rest.length-1].replaceAll("[-+|]", "");
			 System.out.println("QResult is "+QResult+" is this!!!");
			 System.out.println("justfinalres "+justfinalres+" is this!");
	    	 String[] rest2=justfinalres.split("[\\r\\n]+");
	    	 String finalres="";
	    	 for(int restcount=0;restcount<rest2.length;restcount++)
	    	 {
	    	 System.out.println("rest2["+restcount+"] "+rest2[restcount]);
	    	 if(rest2[restcount].contains("("))
	    	 {
	    		 if(!rest2[restcount-1].equals(columnname))
	    		 {
	    		 finalres=rest2[restcount-1].trim();
	    		 
	    		 break;
	    		 }
	    	 }
	    	 
	    	 }
	    	 System.out.println("finalres "+finalres);
	    	 //System.out.println(columnname[col]);
	    	 
	    	 //System.out.println(itemid[item]);
	    	//System.out.println(itemlist.get(item).toString());
	    	//System.out.println(columnname[col]);
	    	 
	    	 if(finalres.equalsIgnoreCase(columnvalue))
	    	 {
	    		 Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname+" from NAS file "+columnvalue, "Matched with the value found in RDS DB "+finalres);
	    		 
	    		 
	    	 }
	    	 else
	    	 {		
	    		 if(finalres.equals("(0 rows)")||finalres.equals(""))
	    		 {
	    			 
	    			 Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname+" for Order "+orderid+" and item "+itemid[item]+" from NAS file "+columnvalue, "Did not match because NO value was found in RDS DB ");
		    		 
	    		 }
	    		 else
	    		 {
	    		 Utilities_i5A_All.utilityFileWriteOP.writeToLog(tcid, "The value of "+columnname+" for Order "+orderid+" and item "+itemid[item]+" from NAS file "+columnvalue, "Did not match with the value found in RDS DB "+finalres);
	    		 }
	    		 res=false;
	    	 }
	    	 
	    	 
	        
	        }
	        
	        out.flush();
	        out.write(("\\q"+"\n").getBytes());
	        
	        out.flush();
	        
	        Thread.sleep(5000);
	      //  String [] res=   QResult.split(" ");

	/*        System.out.println("4");
	   for(int j=0;j<res.length;j++){
	
	   if(McOlls_I5a_RDS_DB.isInteger(res[j])){
		   
	    	System.out.println(res[j].trim());
	    	adj_val_rds=res[j].trim();
	    	
	     }  
	   

	   }*/
	   
	   
	// System.out.println( session.isConnected());

	  channel.disconnect();

	  session.disconnect();
	  
	  Thread.sleep(5000);
	  
	  out.close();
	  
	  in.close();
	  
	//System.out.println(adj_val_rds);
	  
	 

}
	              

	        
catch(Exception e){
e.printStackTrace();
System.out.println(e);

	        } 
	
	
	finally{        
	        
		
		
		//System.out.println(session.isConnected());

		    if (channel != null) {
		      session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
		       // System.out.println(channel.isConnected());
		    }
		
	      return res;
	}

	}
	        
public static void main(String args[]){
	
/*
	try {
		McColls_I5a_Utilities_NAS.TemporaryOrderIDExtract();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	*/
	
}
	
}
