package newConsolidation_WholeSale_CL;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;



//import McColls_EBS_I19_Functions.McColls_I19_GetCall;
//import McColls_I5a_Functions.McColls_I5a_GetCall;
//import McColls_I5a_Functions.McColls_I5a_PostCall;
//import McColls_I5a_Functions.McColls_I5a_Utilities;
//import McColls_I5a_Functions.McColls_I5a_Utilities_NAS;










import Utilities_i5A_All.*;
import I5A_OrderConsolidationUtilities.*;
import McColls_I5a_All_Functions.McColls_I5a_Utilities;

public class TC12_Mcdly_McCollsI5a_Order_with_ref_Code_CL_OrderCreation {
	
	//String URL;
			String DriverPath;
			String DriverName;
			String DriverType;
			String BrowserPath;
			String ServerName;
			String SheetName;
			String ItemDetailsSheetName;
			
			String TestDataPath;
			String TemporaryFilePath;
			
			String ResultPath="";
			
			String TemporaryConsolidationFilePath="";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
		utilityFileWriteOP.writeToLog("*********************************START**********************************");	
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime());
		
		//URL=ProjectConfigurations.LoadProperties("SalesforceAutomation_ProjectURL");
		DriverPath=ProjectConfigurations.LoadProperties("McColls_I5a_DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("McColls_I5a_DriverName");
		DriverType=ProjectConfigurations.LoadProperties("McColls_I5a_DriverType");
		BrowserPath=ProjectConfigurations.LoadProperties("McColls_I5a_BrowserPath");
		ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");
		TestDataPath=ProjectConfigurations.LoadProperties("McColls_I5a_TestDataPath_CL");
		SheetName=ProjectConfigurations.LoadProperties("McColls_I5a_SheetName");
		
		TemporaryFilePath=ProjectConfigurations.LoadProperties("McColls_I5a_TemporaryFilePath");
		
		
	
		
		System.out.println(BrowserPath);
		
		if(ServerName.equalsIgnoreCase("Server1")){
			
			ResultPath=utilityFileWriteOP.ReadResultPathServer1();
		}
		
		
		if(ServerName.equalsIgnoreCase("Server2")){
			
			ResultPath=utilityFileWriteOP.ReadResultPathServer2();
			
		}
	}

	@After
	public void tearDown() throws Exception {
		
		utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime());

		utilityFileWriteOP.writeToLog("*********************************End**********************************");
		
	}

	@Test
	public void test() throws IOException {

		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String Final_Result = "FAIL";
		String TestKeyword = "N_OrderConsolidation_CL";
		
		 McColls_I5a_Utilities.OrderIdGenerationI5aConsolidation(TestDataPath, TestKeyword, "Test Data: OrderID Creation", ResultPath)	;
			
			

		ItemDetailsSheetName=TestKeyword;
		String ProxyHostName = null;
		String ProxyPort = null;
		String SYSUserName = null;
		String SYSPassWord = null;
		
		String TargetHostName = null;
		String TargetPort = null;
		String TargetHeader = null;
		String UrlTail = null;
		String ApiKey = null;
		String AuthorizationKey = null;
		String AuthorizationValue = null;
		
		String AllOrderIDs = null;
		String AllMessageTypes = null;
		String AllShipToLocationIds = null;
		String ShipToDeliverAt = null;
		
		String AllOrderAttributes = null;
		String AllMessageAttributes = null;
		String AllShipToAddressAttributes = null;
		String AllBillToAddressAttributes = null;
		String AllBillToTaxAddressAttributes = null;
		String AllItemAttributes = null;
		
		String SFTPHostName = null;
		String SFTPPort = null;
		String SFTPUserName = null;
		String SFTPPassword = null;
		String NasPath = null;
		
		String ColumnNames = null;
		String ItemIDs = null;
		
		String Quantities="0";
		
		//--------------------Doc File---------------------
				XWPFDocument doc = new XWPFDocument();
		        XWPFParagraph p = doc.createParagraph();
		        XWPFRun xwpfRun = p.createRun();
		        
		        String consolidatedScreenshotpath="";
		        //-------------------------------------------------
		
		String OrderCount="";   // Default value
		


	int r = 0;
		
		
try{

		int rows = 0;
	
		int occurances = 0;
		
		int ItemDetailsoccurances = 0;
		
		Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
	
		Sheet sheet1 = wrk1.getSheet(SheetName);
		rows = sheet1.getRows();
		
		
		System.out.println(rows);
       // CreateExcelFile.CreateTempExcel();


		
		for(r=1; r<rows; r++) {
			
			TestCaseNo = sheet1.getCell(0, r).getContents().trim();
			TestCaseName = sheet1.getCell(1, r).getContents().trim();
			Keyword = sheet1.getCell(2, r).getContents().trim();
			
			
			if(occurances>0){
		        
		    	if(!(TestKeyword.contentEquals(Keyword))){

			    	break;
			    	
		    	}

			 }
			
			
			
			if(Keyword.equalsIgnoreCase(TestKeyword)) {
				  
				 occurances=occurances+1;
				 
			System.out.println(TestKeyword);
				
			
			Sheet sheet2 = wrk1.getSheet(TestKeyword);	 
				 
			
				int NoOfRows= sheet2.getRows();
				
			//	 System.out.println("Hi");
				
				int NoOfColumns= sheet2.getColumns();
				 
				 
			    System.out.println("Total row no in ItemDetails sheet is "+NoOfRows);
			    
			    System.out.println("Total column no in ItemDetails sheet is "+NoOfColumns);
			    

			    String OrderID=null;
			    
			    int Dt  = (int) new Date().getTime();
				
				String intDt=String.valueOf(Dt);
			    
			    String tFileName="TempResultFile/"+intDt+".csv";
	    
   for(int a=1;a<NoOfRows;a++){
	   

	OrderID=sheet2.getCell(0, a).getContents().trim();
	ProxyHostName = sheet2.getCell(1, a).getContents().trim();
	ProxyPort = sheet2.getCell(2, a).getContents().trim();
	SYSUserName = sheet2.getCell(3, a).getContents().trim();
	SYSPassWord = sheet2.getCell(4, a).getContents().trim();
	
	TargetHostName = sheet2.getCell(5, a).getContents().trim();
	
	TargetPort = sheet2.getCell(6, a).getContents().trim();
	
	TargetHeader = sheet2.getCell(7, a).getContents().trim();
	
	UrlTail = sheet2.getCell(8, a).getContents().trim();

	ApiKey = sheet2.getCell(9, a).getContents().trim();
	
	AuthorizationKey = sheet2.getCell(10, a).getContents().trim();
	AuthorizationValue =sheet2.getCell(11, a).getContents().trim();
	
	
	
	
	AllMessageTypes = sheet2.getCell(12, a).getContents().trim();
	
	AllShipToLocationIds = sheet2.getCell(13, a).getContents().trim();
	
	ShipToDeliverAt = sheet2.getCell(14, a).getContents().trim();
				
	AllOrderAttributes = sheet2.getCell(15, a).getContents().trim();

	AllMessageAttributes = sheet2.getCell(16, a).getContents().trim();
	
	AllShipToAddressAttributes = sheet2.getCell(17, a).getContents().trim();
	
	
	AllBillToAddressAttributes = sheet2.getCell(18, a).getContents().trim();
	AllBillToTaxAddressAttributes = sheet2.getCell(19, a).getContents().trim();

	SFTPHostName = sheet2.getCell(20, a).getContents().trim();
	SFTPPort = sheet2.getCell(21, a).getContents().trim();
	SFTPUserName = sheet2.getCell(22, a).getContents().trim();
	SFTPPassword = sheet2.getCell(23, a).getContents().trim();
	NasPath = sheet2.getCell(24, a).getContents().trim();
	
	ColumnNames = sheet2.getCell(25, a).getContents().trim();
	
	String AllItemIDs = sheet2.getCell(26, a).getContents().trim();
	String QuantityType=sheet2.getCell(27, a).getContents().trim();
	String AllShipQty = sheet2.getCell(28, a).getContents().trim();

	String  ExpOrderStatus=null;
	

	OrderCount=sheet2.getCell(29, a).getContents().trim();
	
	String ItemValidationStatus = sheet2.getCell(31, a).getContents().trim();
	
	
	String [] OrderAttributes=AllOrderAttributes.split(",");

	String OrdRefCode=OrderAttributes[2];
	
	
	String [] Itemss=AllItemIDs.split(",");
	
	
	String qtys[]=QuantityType.split(",");
	
	
	String []AllShipQtys=AllShipQty.split(",");
	

	int itemDetailsPrintOccurence = 0;
	
	File screenshotpath = new File(ResultPath+"Screenshot/"+TestCaseNo+"/");
	screenshotpath.mkdirs();
	
	consolidatedScreenshotpath=ResultPath+"Screenshot/"+TestCaseNo+"/"+TestCaseNo+"_"; 
	//-------------------HTML Header--------------------
 
	Reporting_Utilities.writeHeaderToHTMLLog(TestCaseNo, TestCaseName, ResultPath);
						
	//----------------------------------------------------

	 
	
	String []ItemValidationStatusList=ItemValidationStatus.split(",");
	
	
	int trackingNegative=0;
	int trackingPositive=0;
	
	
	for(int t=0;t<ItemValidationStatusList.length;t++){
		

	      if(ItemValidationStatusList[t].contains("N")){
		
	    	  trackingNegative=trackingNegative+1;
	 
	      }
	  
	      if(ItemValidationStatusList[t].contains("P")){
	    		
	    	  trackingPositive=trackingPositive+1;

	      }
	}
		
		if(trackingPositive==ItemValidationStatusList.length){
		
		
			
			
			ExpOrderStatus="confirmed";

		}
			
		
		else{
		
			ExpOrderStatus="shipped-partial";
			
			
		}

	
	

		
	 for(int i=0; i<Itemss.length; i++) {
		 

			
		 String RestGetItemDetails = McColls_I5a_GetCall.GetCallItemDetailsFromWebPortal(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, Itemss[i], "skuPin",qtys[i], String.valueOf(i+1), AllShipQtys[i], TestCaseNo, ResultPath);
		 

		 excelCellValueWrite.writeValueToCell(RestGetItemDetails, a, 32+i, TestDataPath, ItemDetailsSheetName);
		 
		 
  }
	

	
	 Boolean RestPost = McColls_I5a_PostCall.PostCallOrderCreation_Negative_SC(TestDataPath, ItemDetailsSheetName, ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue,OrderID, OrderCount,AllMessageTypes, AllShipToLocationIds, ShipToDeliverAt, AllOrderAttributes, AllMessageAttributes, AllShipToAddressAttributes, AllBillToAddressAttributes, AllBillToTaxAddressAttributes,AllItemIDs, a,TemporaryConsolidationFilePath,Keyword, 
			 
			 tFileName,TestCaseNo,ResultPath,ItemValidationStatus,xwpfRun);
	 
	 
	 
	 if(RestPost==true) {
		 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "Post Order Service Call", "Order Created Successfully.", "PASS", ResultPath);
		}
				
	 else{
		 
		 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "Post Order Service Call", "Error Occured during Order Creation.", "FAIL", ResultPath);
		 
		 throw new MyException("Test Stopped Because of Failure. Please check Execution log");
	
	 
	 }
	 
	 

	Boolean OrderStatus= McColls_I5a_GetCall.GetOrderStatusValidation(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue, OrderID,ExpOrderStatus, TestCaseNo,ResultPath,xwpfRun);
	if(OrderStatus==true) {
		 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "Get Order Status Validation", "Order Status Validated Successfully.", "PASS", ResultPath);
		}
				
	 else{
		 
		 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "2", "Get Order Status Validation", "Error Occured during Order Status Validation.", "FAIL", ResultPath);
		 
		 throw new MyException("Test Stopped Because of Failure. Please check Execution log");
	 }

	 Boolean RestGet = McColls_I5a_GetCall.GetCallItemStatusValidationWithInvalidItems(ProxyHostName, Integer.parseInt(ProxyPort), SYSUserName, SYSPassWord, TargetHostName, Integer.parseInt(TargetPort), TargetHeader, UrlTail, ApiKey, AuthorizationKey, AuthorizationValue,OrderID, "confirmed",ItemValidationStatus, TestCaseNo,ResultPath,xwpfRun);
	
	 
	 if(RestGet==true) {
		 
		 
		 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "3", "Get Item Status Validation", "Item Status Validated Successfully.", "PASS", ResultPath);
		}
				
	 else{
		 
		 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "3", "Get Item Status Validation", "Error Occured during Item Status Validation.", "FAIL", ResultPath);
		 
		 throw new MyException("Test Stopped Because of Failure. Please check Execution log");
	 
	 }
	 

	
	System.out.println("Order ID "+OrderID+"Created");
	
	
	Final_Result = "PASS";
	


	// excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);

	
	 
	 Assert.assertTrue(TestCaseName, true);	

               }
          
			
			}

     }

		
		
	}
		
		
  catch(Exception e) {
			e.printStackTrace();


			 Final_Result = "FAIL";
			 excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);
			 
			 Assert.assertTrue(TestCaseName, false);
	   }
finally
{   

	FileOutputStream out1 = new FileOutputStream(consolidatedScreenshotpath+Final_Result+"1.docx");
	doc.write(out1);
       out1.close();
	 //Reporting_Utilities.writeCloseRowToHTMLLog(TestCaseNo, ResultPath);
       RowGenerator.collateHTMLLog(TestCaseNo, ResultPath);
}
		

	}

}
