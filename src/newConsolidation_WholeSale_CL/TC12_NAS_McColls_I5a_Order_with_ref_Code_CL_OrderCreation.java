package newConsolidation_WholeSale_CL;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Sheet;
import jxl.Workbook;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;














//import McColls_EBS_I19_Functions.McColls_I19_GetCall;
//import McColls_I5a_Functions.McColls_I5a_GetCall;
//import McColls_I5a_Functions.McColls_I5a_PostCall;
//import McColls_I5a_Functions.McColls_I5a_Utilities;
//import McColls_I5a_Functions.McColls_I5a_Utilities_NAS;


















import Utilities_i5A_All.*;
import I5A_OrderConsolidationUtilities.*;
import McColls_I5A_OrderCreation_Utility.Consolidation;
import McColls_I5a_All_CommonFunctions.McColls_I5a_Utilities_NAS;
import McColls_I5a_All_CommonFunctions.McColls_I5a_Utilities.*;
public class TC12_NAS_McColls_I5a_Order_with_ref_Code_CL_OrderCreation {
	
	//String URL;
	String DriverPath;
	String DriverName;
	String DriverType;
	String BrowserPath;
	String ServerName;
	String SheetName;
	String ItemDetailsSheetName;
	
	String TestDataPath;
	String TemporaryFilePath;
	
	String ResultPath="";
	
	String TemporaryConsolidationFilePath="";

@BeforeClass
public static void setUpBeforeClass() throws Exception {
}

@AfterClass
public static void tearDownAfterClass() throws Exception {
}

@Before
public void setUp() throws Exception {

utilityFileWriteOP.writeToLog("*********************************START**********************************");	
utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime());

DriverPath=ProjectConfigurations.LoadProperties("McColls_I5a_DriverPath");
DriverName=ProjectConfigurations.LoadProperties("McColls_I5a_DriverName");
DriverType=ProjectConfigurations.LoadProperties("McColls_I5a_DriverType");
BrowserPath=ProjectConfigurations.LoadProperties("McColls_I5a_BrowserPath");
ServerName=ProjectConfigurations.LoadProperties("AutomationServerName");
TestDataPath=ProjectConfigurations.LoadProperties("McColls_I5a_TestDataPath_CL");
SheetName=ProjectConfigurations.LoadProperties("McColls_I5a_SheetName");

TemporaryFilePath=ProjectConfigurations.LoadProperties("McColls_I5a_TemporaryFilePath");


System.out.println(BrowserPath);

if(ServerName.equalsIgnoreCase("Server1")){
	
	ResultPath=utilityFileWriteOP.ReadResultPathServer1();
}


if(ServerName.equalsIgnoreCase("Server2")){
	
	ResultPath=utilityFileWriteOP.ReadResultPathServer2();
	
}
}

@After
public void tearDown() throws Exception {

utilityFileWriteOP.writeToLog(getCurrentDate.getISTTime());

utilityFileWriteOP.writeToLog("*********************************End**********************************");

}

@Test
public void test() throws IOException {



//HashMap<String  , String>orderID= new HashMap<String, String>();	

Sheet sheet2=null;
String TestCaseNo = null;
String TestCaseName = null;
String Keyword = null;
String Final_Result = "FAIL";
String TestKeyword = "N_OrderConsolidation_CL";

Boolean resConsolidation_NegSC=false;


ItemDetailsSheetName=TestKeyword;
String ProxyHostName = null;
String ProxyPort = null;
String SYSUserName = null;
String SYSPassWord = null;

String TargetHostName = null;
String TargetPort = null;
String TargetHeader = null;
String UrlTail = null;
String ApiKey = null;
String AuthorizationKey = null;
String AuthorizationValue = null;

String AllOrderIDs = null;
String AllMessageTypes = null;
String ShipToLocationIds = null;
String ShipToDeliverAt = null;

String AllOrderAttributes = null;
String AllMessageAttributes = null;
String AllShipToAddressAttributes = null;
String AllBillToAddressAttributes = null;
String AllBillToTaxAddressAttributes = null;
String AllItemAttributes = null;

String SFTPHostName = null;
String   SFTPPort =null;
String SFTPUserName = null;
String SFTPPassword = null;
String NasPath = null;

String ColumnNames = null;
String ItemIDs = null;

String Quantities="0";

String OrderCount="";   // Default value

String CustomerID="mccolls";
String ItemTypeList="";

String TempOrderID_NAS=null;

 

//--------------------Doc File---------------------
XWPFDocument doc = new XWPFDocument();
XWPFParagraph p = doc.createParagraph();
XWPFRun xwpfRun = p.createRun();

String consolidatedScreenshotpath="";
//-------------------------------------------------

int r = 0;


try{

int rows = 0;

int occurances = 0;

int ItemDetailsoccurances = 0;

Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));

Sheet sheet1 = wrk1.getSheet(SheetName);
rows = sheet1.getRows();

// CreateExcelFile.CreateTempExcel();



for(r=1; r<rows; r++) {
	
	TestCaseNo = sheet1.getCell(0, r).getContents().trim();
	TestCaseName = sheet1.getCell(1, r).getContents().trim();
	Keyword = sheet1.getCell(2, r).getContents().trim();
	
	
	if(occurances>0){
        
    	if(!(TestKeyword.contentEquals(Keyword))){

	    	break;
	    	
    	}

	 }
	
	
	
	if(Keyword.equalsIgnoreCase(TestKeyword)) {
		  
		 occurances=occurances+1;
		 
		 
		sheet2 = wrk1.getSheet(TestKeyword);
		

		
		
		Sheet sheet3 = wrk1.getSheet("Env");	 
		 
		 
		int NoOfRows= sheet2.getRows();
		int NoOfColumns= sheet2.getColumns();
		 
		 
	    System.out.println("Total row no in ItemDetails sheet is "+NoOfRows);
	    
	    System.out.println("Total column no in ItemDetails sheet is "+NoOfColumns);
	    

	    String OrderID=null;
	    
	    
	  //  NoOfRows=2;
	    
	    
String  TempFilePath= sheet3.getCell(0, 0).getContents().trim();


for(int a=1;a<NoOfRows;a++){


OrderID=sheet2.getCell(0, a).getContents().trim();
ProxyHostName = sheet2.getCell(1, a).getContents().trim();
ProxyPort = sheet2.getCell(2, a).getContents().trim();
SYSUserName = sheet2.getCell(3, a).getContents().trim();
SYSPassWord = sheet2.getCell(4, a).getContents().trim();

TargetHostName = sheet2.getCell(5, a).getContents().trim();

TargetPort = sheet2.getCell(6, a).getContents().trim();

TargetHeader = sheet2.getCell(7, a).getContents().trim();

UrlTail = sheet2.getCell(8, a).getContents().trim();

ApiKey = sheet2.getCell(9, a).getContents().trim();

AuthorizationKey = sheet2.getCell(10, a).getContents().trim();
AuthorizationValue =sheet2.getCell(11, a).getContents().trim();

AllMessageTypes = sheet2.getCell(12, a).getContents().trim();

ShipToLocationIds = sheet2.getCell(13, a).getContents().trim();

ShipToDeliverAt = sheet2.getCell(14, a).getContents().trim();
		
AllOrderAttributes = sheet2.getCell(15, a).getContents().trim();

AllMessageAttributes = sheet2.getCell(16, a).getContents().trim();

AllShipToAddressAttributes = sheet2.getCell(17, a).getContents().trim();
AllBillToAddressAttributes = sheet2.getCell(18, a).getContents().trim();
AllBillToTaxAddressAttributes = sheet2.getCell(19, a).getContents().trim();

SFTPHostName = sheet2.getCell(20, a).getContents().trim();
SFTPPort = sheet2.getCell(21, a).getContents().trim();
SFTPUserName = sheet2.getCell(22, a).getContents().trim();
SFTPPassword = sheet2.getCell(23, a).getContents().trim();
NasPath = sheet2.getCell(24, a).getContents().trim();

ColumnNames = sheet2.getCell(25, a).getContents().trim();

ItemTypeList= sheet2.getCell(31, a).getContents().trim();



String AllItemIDs = sheet2.getCell(26, a).getContents().trim();


ShipToLocationIds=sheet2.getCell(30, a).getContents().trim();


String [] OrderAttributes=AllOrderAttributes.split(",");

String OrdRefCode=OrderAttributes[2];


File screenshotpath = new File(ResultPath+"Screenshot/"+TestCaseNo+"/");
screenshotpath.mkdirs();


consolidatedScreenshotpath=ResultPath+"Screenshot/"+TestCaseNo+"/"+TestCaseNo+"_"; 



HashMap<String, String> hm = null;


hm = new HashMap<String, String>();



//hm=Consolidation.LoadItemsNAS("TempResultFile/ConsolidationLog.csv",hm);
hm=Consolidation.LoadItemsNAS(TempFilePath,hm);


System.out.println("Hash Map is "+hm);




int ItemCount= hm.size();


System.out.println(ItemCount);

System.out.println(hm);

String LocalPath="TempDataFiles/";


List<String> l = new ArrayList<String>(hm.keySet());


// System.out.println("HiiiiikkkkTest");

// String [] itemsList=null;

String[] itemsList = new String[l.size()];
	
	
for(int p1=0;p1<l.size();p1++){
		
		
itemsList[p1]=l.get(p1);

//  System.out.println("Hiiiii"+itemsList[p1]);

}



// System.out.println("ItemList is"+itemsList[0]);
//-------------------HTML Header--------------------

Reporting_Utilities.writeHeaderToHTMLLog(TestCaseNo, TestCaseName, ResultPath);
					
//----------------------------------------------------

ArrayList<HashMap<String, String>> TemporaryOrderIDExtract = McColls_I5a_Utilities_NAS.NASContent_RetriveNegativeSc(SFTPHostName, Integer.parseInt(SFTPPort), SFTPUserName, SFTPPassword, NasPath,ShipToDeliverAt,LocalPath,OrdRefCode, ShipToLocationIds, l, ItemTypeList,AllItemIDs,TestCaseNo);

System.out.println( TemporaryOrderIDExtract.size());




String [] ItemsAll_Type=ItemTypeList.split(",");

String [] ItemsAll=AllItemIDs.split(",");

String[] ItemsListValid = new String[50];


for(int k=0;k<ItemsAll_Type.length;k++){


if(ItemsAll_Type[k].contentEquals("P")){


ItemsListValid[k]="P";


   }

}


//int NoOfRecordsInNAS=0;

int NoOfValidItems=ItemsAll_Type.length;


if(NoOfValidItems==itemsList.length){


// utilityFileWriteOP.writeToLog(TestCaseNo, "Invalid Items not Present in the Consolidated File as expected ","PASS");

resConsolidation_NegSC=true;



}



else {



//utilityFileWriteOP.writeToLog(TestCaseNo, "Invalid Items are  Present in the Consolidated File  ","FAIL");

resConsolidation_NegSC=false;


}








int cnt=0;

int PassCount=0;

for(int p1=0;p1<itemsList.length;p1++){



for(int u=0;u<TemporaryOrderIDExtract.size();u++){

	

String its=TemporaryOrderIDExtract.get(u).get("itemId").toString();



if(itemsList[p1].contentEquals(its)){

// shipToLocationId ,CNV ,shipToDeliverAt,itemId,itemAlternateId,itemDescription,quantityType,quantityOrdered

String qty=TemporaryOrderIDExtract.get(u).get("quantityOrdered").toString();

/*	
* 
* 
* 
* //System.out.println("Qty Ordered is "+hm.get(its).toString());
*/

String shipToLocationId_NAS=TemporaryOrderIDExtract.get(u).get("shipToLocationId").toString();

String messageId_NAS=TemporaryOrderIDExtract.get(u).get("messageId").toString();

//String itemDescription_NAS=TemporaryOrderIDExtract.get(u).get("itemDescription").toString();

String orderReferenceCode_NAS=TemporaryOrderIDExtract.get(u).get("orderReferenceCode").toString();

String quantities_NAS=TemporaryOrderIDExtract.get(u).get("quantityOrdered").toString();

TempOrderID_NAS=TemporaryOrderIDExtract.get(u).get("orderId").toString();

String customerId_NAS=TemporaryOrderIDExtract.get(u).get("customerId").toString();

String quantityType_NAS=TemporaryOrderIDExtract.get(u).get("quantityType").toString();

String ShipToDeliverAt_NAS=TemporaryOrderIDExtract.get(u).get("shipToDeliverAt").toString();

String createdAt_NAS=TemporaryOrderIDExtract.get(u).get("createdAt").toString();


String itemDescription_NAS=TemporaryOrderIDExtract.get(u).get("itemDescription").toString();

  
String itemAlternateId_NAS=TemporaryOrderIDExtract.get(u).get("itemAlternateId").toString();

String [] BarcodeEAN=	itemAlternateId_NAS.split("\\|");   

/*	
String itemAlternateId_NAS=TemporaryOrderIDExtract.get(u).get("itemDescription").toString();


//barcodeEan:5010000000000|clientId:wholesale|skuPin:108904757|skuMin:108904407|skuLegacy:0400040

String [] ItemAlternateID=	itemAlternateId_NAS.split("\\|");




String []  clientIds=ItemAlternateID[1].split(":");

String clientId_NAS=clientIds[1];
*/



if(u==0){
	   
	
	
	for(  int k=1;k<sheet2.getRows();k++){

		   String OrderIDP=sheet2.getCell(0, k).getContents().trim(); 
		   
		   boolean resTempOrderIDValidation=McColls_I5a_Utilities.ValidateTemporaryOrderIDMAPWithOrderIDInRDS(TempOrderID_NAS,OrderIDP, TestCaseNo,ResultPath,xwpfRun);
    	   
   		
   		if(resTempOrderIDValidation==true){

   			//PassCount=PassCount+1;

   		 utilityFileWriteOP.writeToLog(TestCaseNo, "Temporary Order ID   "+ TempOrderID_NAS +"is mapped with  "+OrderIDP+"in RDS","",ResultPath,xwpfRun,"");

   		}  

   		else {
   		
   		 utilityFileWriteOP.writeToLog(TestCaseNo, "Temporary Order ID   "+ TempOrderID_NAS +"is not mapped with  "+OrderIDP+"in RDS","",ResultPath,xwpfRun,"");

   		}

	}  

} 


//2018-02-26


 String DATE_FORMAT = "yyyy-dd-mm";
    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
  //  System.out.println("Formated Date " + sdf.format(date));

    
    
    
   System.out.println(ShipToDeliverAt_NAS);

   
   //System.out.println(createdAt_NAS);
    
   // createdAt_NAS=sdf.format(createdAt_NAS);
    
    
// String Exp_CreatedAt=sdf.format(new Date());
    
    

    
    
/*    
    if(Exp_CreatedAt.contentEquals(createdAt_NAS)){
   		 
   		 PassCount=PassCount+1;

     System.out.println("Message CeatedAt in NAS  "+ createdAt_NAS +"matches with "+Exp_CreatedAt);

     utilityFileWriteOP.writeToLog(TestCaseNo, "Message CeatedAt in NAS   "+ createdAt_NAS +"matches with "+Exp_CreatedAt, "Success");
	 
   }


   else {

	 System.out.println("Message CeatedAt in NAS  "+ createdAt_NAS +"not matches with "+Exp_CreatedAt);

	     utilityFileWriteOP.writeToLog(TestCaseNo, "Message CeatedAt in NAS   "+ createdAt_NAS +"not matches with "+Exp_CreatedAt, "FAIL");
		 
   
   
   
 } 		    
    
*/


   
   
   
/*
   if(clientId_NAS.contentEquals("wholesale")){
   		 
   		 PassCount=PassCount+1;

     System.out.println("clientId in NAS  "+ clientId_NAS +"matches with "+"wholesale");

     utilityFileWriteOP.writeToLog(TestCaseNo, "clientId in NAS  "+ clientId_NAS +"matches with wholesale", "Success");
	 
     
     
     utilityFileWriteOP.writeToLog(TestCaseNo, "clientId in NAS   "+ clientId_NAS +"matches with wholesale","",ResultPath,xwpfRun,"");

   }


   else {

	   System.out.println("clientId in NAS  "+ clientId_NAS +"not matches with "+"wholesale");

	     utilityFileWriteOP.writeToLog(TestCaseNo, "clientId in NAS  "+ clientId_NAS +"not matches with wholesale", "FAIL");
		 
	     
	     
	     utilityFileWriteOP.writeToLog(TestCaseNo, "clientId in NAS   "+ clientId_NAS +"not matches with wholesale","",ResultPath,xwpfRun,"");

	     
   } 
  */ 
   
   
   
   if(BarcodeEAN[1].contentEquals("clientId:wholesale")){
 		 
 		 PassCount=PassCount+1;

   System.out.println("clientId in NAS  "+ BarcodeEAN[1] +"matches with "+"clientId:wholesale");

   utilityFileWriteOP.writeToLog(TestCaseNo, "clientId in NAS   "+ BarcodeEAN[1] +"matches with clientId:wholesale", "Success");

   utilityFileWriteOP.writeToLog(TestCaseNo, "clientId in NAS   "+ BarcodeEAN[1] +"matches with clientId:wholesale","",ResultPath,xwpfRun,"");


	   
	   }


 else {

	   System.out.println("clientId in NAS  "+ BarcodeEAN[1] +"not matches with "+"clientId:wholesale");

	     utilityFileWriteOP.writeToLog(TestCaseNo, "clientId in NAS   "+ BarcodeEAN[1] +"Not matches with clientId:wholesale", "Fail");

	     utilityFileWriteOP.writeToLog(TestCaseNo, "clientId in NAS   "+ BarcodeEAN[1] +"Not matches with clientId:wholesale","",ResultPath,xwpfRun,"");

	  
 } 
   
   
   

if(BarcodeEAN[0].contentEquals("barcodeEan:wholesale")){
  	   		 
  	   		 PassCount=PassCount+1;

  	     System.out.println("barcodeEan in NAS  "+ BarcodeEAN[0] +"matches with "+"barcodeEan:wholesale");

  	     utilityFileWriteOP.writeToLog(TestCaseNo, "barcodeEan in NAS   "+ BarcodeEAN[0] +"matches with barcodeEan:wholesale", "Fail");
  		 
  	     
  	     
  	     utilityFileWriteOP.writeToLog(TestCaseNo, "barcodeEan in NAS   "+ BarcodeEAN[0] +"matches with barcodeEan:wholesale","",ResultPath,xwpfRun,"");

  	  
    	   
    	   }


  	   else {

  		 System.out.println("barcodeEan in NAS  "+ BarcodeEAN[0] +"not matches with "+"barcodeEan:wholesale");

  	     utilityFileWriteOP.writeToLog(TestCaseNo, "barcodeEan in NAS   "+ BarcodeEAN[0] +"not matches with barcodeEan:wholesale", "Success");
  		 
  	     
  	     
  	     utilityFileWriteOP.writeToLog(TestCaseNo, "barcodeEan in NAS   "+ BarcodeEAN[0] +"not matches with barcodeEan:wholesale","",ResultPath,xwpfRun,"");

  	   }
   

   
   
   if(itemDescription_NAS.contentEquals("Optional")){
   		 
   		 PassCount=PassCount+1;

     System.out.println("itemDescription in NAS  "+ itemDescription_NAS +"matches with "+"Optional");

     utilityFileWriteOP.writeToLog(TestCaseNo, "itemDescription in NAS  "+ itemDescription_NAS +"matches with Optional", "Success");
	 
     
     
     utilityFileWriteOP.writeToLog(TestCaseNo, "itemDescription in NAS  "+ itemDescription_NAS +"matches with Optional","",ResultPath,xwpfRun,"");

   }


   else {

	   System.out.println("itemDescription in NAS  "+ itemDescription_NAS +" not matches with "+"Optional");

	     utilityFileWriteOP.writeToLog(TestCaseNo, "itemDescription in NAS  "+ itemDescription_NAS +"Not matches with Optional", "FAIL");
		 
	     
	     
	     utilityFileWriteOP.writeToLog(TestCaseNo, "itemDescription in NAS  "+ itemDescription_NAS +"Not matches with Optional","",ResultPath,xwpfRun,"");

	     
   }  	   

    

if(ShipToDeliverAt.contentEquals(ShipToDeliverAt_NAS)){
	 
	 PassCount=PassCount+1;

System.out.println("ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"matches with "+ShipToDeliverAt);

utilityFileWriteOP.writeToLog(TestCaseNo, "ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"matches with "+ShipToDeliverAt, "Success");



utilityFileWriteOP.writeToLog(TestCaseNo, "ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"matches with "+ShipToDeliverAt,"",ResultPath,xwpfRun,"");





}


else {

System.out.println("ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"not matches with "+ShipToDeliverAt);

 utilityFileWriteOP.writeToLog(TestCaseNo, "ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"not matches with "+ShipToDeliverAt, "FAIL");
 
 utilityFileWriteOP.writeToLog(TestCaseNo, "ShipToDeliverAt in NAS  "+ ShipToDeliverAt_NAS +"not matches with "+ShipToDeliverAt,"",ResultPath,xwpfRun,"");
 
 
 
 
 
} 	




if(OrdRefCode.contentEquals(orderReferenceCode_NAS)){
	 
	 PassCount=PassCount+1;

System.out.println("orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"matches with "+OrdRefCode);

utilityFileWriteOP.writeToLog(TestCaseNo, "orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"matches with "+OrdRefCode, "Success");

utilityFileWriteOP.writeToLog(TestCaseNo, "orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"matches with "+OrdRefCode,"",ResultPath,xwpfRun,"");





}


else {

 utilityFileWriteOP.writeToLog(TestCaseNo, "orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"not matches with "+OrdRefCode, "Fail");
 System.out.println("orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"not matches with "+OrdRefCode);

  utilityFileWriteOP.writeToLog(TestCaseNo, "orderReferenceCode in NAS  "+ orderReferenceCode_NAS +"not matches with "+OrdRefCode,"",ResultPath,xwpfRun,"");
 
} 



if(CustomerID.contentEquals(customerId_NAS)){
 
 PassCount=PassCount+1;

System.out.println("CustomerID in NAS  "+ customerId_NAS +"matches with "+CustomerID);


utilityFileWriteOP.writeToLog(TestCaseNo, "CustomerID in NAS  "+ customerId_NAS +"matches with "+CustomerID , "Success");

utilityFileWriteOP.writeToLog(TestCaseNo, "CustomerID in NAS "+ customerId_NAS +"matches with "+CustomerID,"",ResultPath,xwpfRun,"");



}


else {

System.out.println("CustomerID in NAS  "+ CustomerID +"not matches with "+CustomerID);
utilityFileWriteOP.writeToLog(TestCaseNo, "CustomerID in NAS  "+ customerId_NAS +"not matching  with "+CustomerID , "Fail");
utilityFileWriteOP.writeToLog(TestCaseNo, "CustomerID in NAS "+ customerId_NAS +"not matches with "+CustomerID,"",ResultPath,xwpfRun,"");




}   	



if(ShipToLocationIds.contentEquals(shipToLocationId_NAS)){


PassCount=PassCount+1;
System.out.println("shipToLocationId in NAS  "+ shipToLocationId_NAS +"  matches with "+ShipToLocationIds);
	
	
utilityFileWriteOP.writeToLog(TestCaseNo, "shipToLocationId in NAS  "+ shipToLocationId_NAS +"  matches with "+ShipToLocationIds , "Success");

utilityFileWriteOP.writeToLog(TestCaseNo, "shipToLocationId in NAS  "+ shipToLocationId_NAS +"matches with "+ShipToLocationIds,"",ResultPath,xwpfRun,"");




}


else {
System.out.println("shipToLocationId in NAS  "+ shipToLocationId_NAS +"  not matches with "+ShipToLocationIds);

utilityFileWriteOP.writeToLog(TestCaseNo, "shipToLocationId in NAS  "+ shipToLocationId_NAS +"  not matching  with "+ShipToLocationIds , "Fail");
	 
utilityFileWriteOP.writeToLog(TestCaseNo, "shipToLocationId in NAS  "+ shipToLocationId_NAS +"not matches with "+ShipToLocationIds,"",ResultPath,xwpfRun,"");



}



if(quantityType_NAS.contentEquals("CS")){
	
	
 PassCount=PassCount+1;
System.out.println("quantityType in NAS  "+ quantityType_NAS +"  matches with "+"CS");
	
	
utilityFileWriteOP.writeToLog(TestCaseNo, "quantityType in NAS  "+ quantityType_NAS +"  matches with "+"CS" , "Success");



utilityFileWriteOP.writeToLog(TestCaseNo, "quantityType in NAS  "+ quantityType_NAS +"matches with "+"CS","",ResultPath,xwpfRun,"");




}


else {
System.out.println("quantityType in NAS  "+ quantityType_NAS +"  not matches with "+"CS");

utilityFileWriteOP.writeToLog(TestCaseNo, "quantityType in NAS  "+ quantityType_NAS +"  not matching  with "+"CS" , "Fail");



utilityFileWriteOP.writeToLog(TestCaseNo, "quantityType in NAS  "+ quantityType_NAS +"not matches with "+"CS","",ResultPath,xwpfRun,"");


}



/*

if(AllMessageTypes.contentEquals(messageId_NAS)){

 PassCount=PassCount+1;

System.out.println("messageId in NAS  "+ messageId_NAS +"matches with "+AllMessageTypes);

utilityFileWriteOP.writeToLog(TestCaseNo, "messageId in NAS  "+ messageId_NAS +"matches with "+AllMessageTypes , "Success");
	
utilityFileWriteOP.writeToLog(TestCaseNo, "messageId in NAS "+ messageId_NAS +"matches with "+AllMessageTypes,"",ResultPath,xwpfRun,"");




}


else {
System.out.println("messageId in NAS  "+ messageId_NAS +"not matches with "+AllMessageTypes);
utilityFileWriteOP.writeToLog(TestCaseNo, "messageId in NAS  "+ messageId_NAS +"not matching  with "+AllMessageTypes , "Fail");

utilityFileWriteOP.writeToLog(TestCaseNo, "messageId in NAS "+ messageId_NAS +"not matches with "+AllMessageTypes,"",ResultPath,xwpfRun,"");


}


*/

String  exp_qty=hm.get(its).toString();


String CS_QtyType= Consolidation.getCaseSize_And_ItemType(TempFilePath, its);


String [] CS_QtyTypeArray=CS_QtyType.split("#");




if((CS_QtyTypeArray[0]).contains("EA")){
 
int Qt=0; 
 
 
int Qty=Integer.parseInt(exp_qty);

//System.out.println(" Current item  qty is "+Qty);


int CaseSize=Integer.parseInt(CS_QtyTypeArray[1]);

	//	System.out.println("Current item  case size is "+CaseSize);



Qt= ((Qty +CaseSize - 1) / CaseSize);
	 
exp_qty=String.valueOf(Qt);
	 
}






if(exp_qty.contentEquals(qty)){

	
PassCount=PassCount+1;
	 
System.out.println("Consolidated qty  for Item "+its+"is   "   +qty+" and it matches with expected quantity "+exp_qty);

utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated qty  for Item "+its+"is"   +qty+" and it matches with expected quantity "+exp_qty , "Success");

utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated qty  for Item "+its+"is"   +qty+" and it matches with expected quantity "+exp_qty,"",ResultPath,xwpfRun,"");



}


else {

System.out.println("Consolidated qty  for Item "+its+"is"   +qty+" and it is not matching  with expected quantity "+exp_qty);

utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated qty  for Item "+its+"is"   +qty+" and it is not matching with expected quantity "+exp_qty , "Fail");

utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated qty  for Item "+its+"is"   +qty+" and is not matching  with expected quantity "+exp_qty,"",ResultPath,xwpfRun,"");


}


break;

}

}

}


if((PassCount==(9*itemsList.length))){


//   if((PassCount==(6*itemsList.length))&&(resConsolidation_NegSC)){   
Final_Result = "PASS";

 Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "NAS File Validation", "NAS File Validated Successfully.", "PASS", ResultPath);


 
 excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);

 Assert.assertTrue(TestCaseName, true);	
 
 utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated file validation ", "Success");
 
 System.out.println("Consolidated file validation PASS");

 utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated file validation PASS","",ResultPath,xwpfRun,"");
 
 
 
//  TempOrderID_NAS
 
//    orderIDD
 
 



}


else{





Final_Result = "FAIL";
 
Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, "1", "NAS File Validation", "Error Occured during NAS File Validation.", "FAIL", ResultPath);

excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);

Assert.assertTrue(TestCaseName, false);	

utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated file validation ", "FAIL");

System.out.println("Consolidated file validation FAIL");


utilityFileWriteOP.writeToLog(TestCaseNo, "Consolidated file validation FAIL","",ResultPath,xwpfRun,"");



  } 

 }

}

}
}	

catch(Exception e) {
	e.printStackTrace();


	 Final_Result = "FAIL";
	 excelCellValueWrite.writeValueToCell(Final_Result, r, 3, TestDataPath, SheetName);
	 
	 Assert.assertTrue(TestCaseName, false);
}

finally

{    
System.out.println("Word doc: "+(System.getProperty("user.dir")+"/"+consolidatedScreenshotpath+Final_Result+"2.docx"));	
FileOutputStream out1 = new FileOutputStream(System.getProperty("user.dir")+"/"+consolidatedScreenshotpath+Final_Result+"2.docx");
doc.write(out1);
out1.close();

//Reporting_Utilities.writeCloseRowToHTMLLog(TestCaseNo, ResultPath);
RowGenerator.collateHTMLLog(TestCaseNo, ResultPath);

}


}

}
