package McColls_I5A_OrderCreation_Utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class Consolidation {

	
	public static HashMap<String, String> LoadItemsNAS(String  fin,HashMap<String, String> hm) {
		
		// this function will return hashmap of itemno as key and consolidated qty
		
		BufferedReader br =null;
		
		try{
		
		File file = new File(fin);
		
		
		FileInputStream fis = new FileInputStream(fin);
	 
		
		
		//Construct BufferedReader from InputStreamReader
		br = new BufferedReader(new InputStreamReader(fis));
	 
		String line = null;
		
		
		
	/*	
		int Qty=Integer.parseInt(Exp_quantityOrderedJson);

		System.out.println(" Current item  qty is "+Qty);


		int CaseSize=Integer.parseInt(Exp_itemCaseSize);

		System.out.println("Current item  case size is "+CaseSize);



		int Qt= ((Qty +CaseSize - 1) / CaseSize);

		
		*/
		
		
		
		
		
while ((line = br.readLine()) != null) {
			
			
	System.out.println(line);
			
    String []Consolidation=	line.split(",");
			
			

			
	int  oldQty=0;
	
	String CaseSize=Consolidation[3];
	
	String Type=Consolidation[2];
			
	if(hm.get(Consolidation[0])!=null){
			
			
	oldQty=Integer.parseInt(hm.get(Consolidation[0]));// retrieve old value
			
			
	    }
	
	
	int newQty=Integer.parseInt(Consolidation[1]);
	

	 
	int FinalQty=oldQty+newQty;
	
/*	
	if(Consolidation[2].contentEquals("EA")){
		
		
	  String CaseSize=Consolidation[3];	
		
		
	  int Qty=FinalQty;
	  
	  int CaseSizeItem=Integer.parseInt(CaseSize);
	  
	  
	  int Qt= ((Qty +CaseSizeItem - 1) / CaseSizeItem);
		
	  FinalQty=Qt;
	  
		
	}
	
	
	*/
	
	
	String Final_qty=String.valueOf(FinalQty);
	
	//String testStr=Final_qty+","+CaseSize+","+Type;
	
	
	hm.put(Consolidation[0], Final_qty);
		

	  }  
		
		
	}
		
	catch(Exception e){    
			
			
		System.out.println(e);
			
			
	}
		
		finally{
		
			
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return hm;	
			
			
		}
	
	}
	

	
	
public static String  getCaseSize_And_ItemType(String  fin,String Item) {
		
		BufferedReader br =null;
		
		String Qty_Casse=null;
		
		try{
		
		File file = new File(fin);
		
		
		FileInputStream fis = new FileInputStream(fin);
	 
		
		
		//Construct BufferedReader from InputStreamReader
		br = new BufferedReader(new InputStreamReader(fis));
	 
		String line = null;
		

		
		
		
		while ((line = br.readLine()) != null) {
			
			
	System.out.println(line);
			
    String []Consolidation=	line.split(",");
    
    String itemNo=Consolidation[0];
    
    
    String CaseSize=null;
    
    String QtyType=null;
    
    
    
  if(Item.contentEquals(itemNo)){
			
			
	  CaseSize=Consolidation[2];
    
	  QtyType=Consolidation[3];
	  
	  
	  Qty_Casse=CaseSize+"#"+QtyType;
	  
	  
      break;
	  
	  
    }
    
    
    
    
    
    
    
    
	
		
	}  }
		
	catch(Exception e){    
			
			
		System.out.println(e);
			
			
	}
		
		finally{
			
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		return Qty_Casse;	
			
			
		}
	

		}

	
	
	
	
	
public static void main(String[] args) throws IOException {
		
	
	  HashMap<String, String> hm = null;

					
		hm = new HashMap<String, String>();
		
		/*hm.put("Exp_customerId", Exp_customerId);
		hm.put("Exp_OrderId", Exp_OrderId);
		hm.put("Exp_MessageType", Exp_MessageType);
		hm.put("Exp_ShipToLocationId", Exp_ShipToLocationId);
		hm.put("Exp_ShipToDeliverAt", Exp_ShipToDeliverAt);
		hm.put("Exp_OrdRefCode", Exp_OrdRefCode);
		hm.put("Exp_quantityOrdered", itemListQty[k]);
		hm.put("Exp_itemId", itemList[k]);*/
		
	Consolidation.LoadItemsNAS("TempResultFile/ConsolidationLog.csv",hm);
		
		
     System.out.println(hm);

	}

}
