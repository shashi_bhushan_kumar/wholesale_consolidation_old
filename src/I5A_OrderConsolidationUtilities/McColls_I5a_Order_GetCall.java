package I5A_OrderConsolidationUtilities;

import java.io.IOException;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;




import Utilities_i5A_All.utilityFileWriteOP;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class McColls_I5a_Order_GetCall {
	
	public static boolean GetCallItemStatusValidation_Invalid(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid) throws IOException {
		
		boolean res = false;
		
		
		System.out.println(statusCurrentValue);
		
		String [] shipStatusArrey=statusCurrentValue.split(",");
		
		statusCurrentValue=shipStatusArrey[0];
		
		String ShipOrderStatus=shipStatusArrey[1];
		
		int MaxwaitingTime = 60000;
		
		int regularwaitTime = 10000;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Order ID is " + OrderID);
	        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done");
	        
	        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
	        	
	        	Thread.sleep(regularwaitTime);
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode());
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        
	        
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		        String statusValidationResult = jsonobject.get("statusValidation").toString();
		        
		        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
		        
		        System.out.println("StatusValidation Result is " + statusValidationResult);
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne == 9) {
		        	res = true;
		        	System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("Count of 1 is 9");
		        }
		        else {
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));

		        	break;
		        }
		        
		        if(statusCurrent.equals(statusCurrentValue)) {
		        	res = true;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("statusCurrent is " +statusCurrent);
					
				break;
		        }
		        
		        else{
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("statusCurrent is " +statusCurrent);
		        	
		        }
	        	
	        }
	        	  
	        
	        if(res==true) {
	        	break;
	        }
	        
	        }
	    	
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			return res;
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
	    
		
	}
	
	
	
public static boolean GetOrderStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid) throws IOException {
		
		boolean res = false;
		
		
		String [] shipStatusArrey=statusCurrentValue.split(",");
		
		statusCurrentValue=shipStatusArrey[0];

		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "?" + ApiKey);
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        
	        
	        for (int j=0;j<40;j++){
	        
	        
	       Thread.sleep(10000);
	        	
	        	
	        System.out.println("Order ID is " + OrderID);
	        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done");

	        response = httpclient.execute(target, httpget);

	        
	        
	      //  System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        
	        
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode());
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        
	        
	        
	        
	      //  System.out.println(jsonresponse);
	        
	        
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("orders");
	        
	        JsonObject jsonobject1=jarray.get(0).getAsJsonObject();
	       
	        
	     //   JsonArray jarray = jsonobject.getAsJsonArray("items");
	      //  System.out.println(jarray.size());
	     //   for(int i=0; i<jarray.size(); i++) {
	        
	      //  System.out.println(jsonobject.toString());
	        
	        
	        //utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
	        
	       // JsonArray jarray = jsonobject.getAsJsonArray("orders");
	        
	        
	    //  System.out.println(jarray);
	       
	    
		        
	        	
	        //String statusValidationResult = jsonobject.get("statusValidation").toString();
		        
		        String statusCurrent = jsonobject1.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
		        
		        //System.out.println("StatusValidation Result is " + statusValidationResult);

		        if(statusCurrent.equals(statusCurrentValue)) {
		        	res = true;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success");
					System.out.println("statusCurrent is " +statusCurrent);
					
				     break;
				
		        }
		        
		        else{
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent,"FAIL");
					System.out.println("statusCurrent is " +statusCurrent);
					
					continue;
		        	
		        }
	        	
	       
	        }
	        
	      
	    	
		}  //end of try
	    
	    
	    
	    catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			
			
			//return res;
		}
	    
	    finally{
	        
	     response.close();
	     
	     return res;
	        
	}
	    
	    
	    
		
	}
	
	
	
	
	
	
	
	
	
	
	
public static boolean GetCallItemStatusValidation(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid) throws IOException {
		
		boolean res = false;
		
	
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Order ID is " + OrderID);
	        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done");
	        
	        for(int j =1; j<= 10; j ++) {
	        	
	        	
	        Thread.sleep(10000);
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	      
	        
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode());
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        
	        
	        System.out.println(jsonresponse);
	        
	        
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	      
	        
	        //System.out.println(jsonobject.toString());
	       
	        
	        
	     //   utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
	        
	       
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        
	        int stCnt=jarray.size();
	        
	        
	     //   System.out.println("Json Array Size  for get call is "+stCnt);
	        
	        
	        int mtchCnt=0;
	        
	      
	        System.out.println(jarray.size());
	       
	        
	        
	        
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		        String statusValidationResult = jsonobject.get("statusValidation").toString();
		        
		        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
		        
		        System.out.println("StatusValidation Result is " + statusValidationResult);
		        
		        
		        utilityFileWriteOP.writeToLog(tcid, "StatusValidation Result for Item  ::  "+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", "")+" IS ..", statusValidationResult);
		        

		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne == 9) {
		        	res = true;
		        	System.out.println("Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("Count of 1 is 9");
		        }
		        else {
		        	
		        	res = false;
					
		        	
		        	utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));

		        	//break;
		        }
		        
		        if(statusCurrent.equals(statusCurrentValue)) {
		        	res = true;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("statusCurrent is " +statusCurrent);
					
					mtchCnt=mtchCnt+1;
					
					
				//break;
		        }
		        
		        else{
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("statusCurrent is " +statusCurrent);
		        	
		        }
	        	
	        }
	        	  
	       
	        if(mtchCnt==stCnt) {
	        	break;
	        
	        }
	        
	   }
	    	
} 
	    
	    
catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			
			
		}
	    
	    finally{
	        
	        response.close();
	        
	        return res;
	        
	   }

	}
	
	


public static boolean GetOrderStatus(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid) throws IOException {
	
	boolean res = false;
	

	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        
        
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done");
        
        
        
        for(int j =1; j<= 10; j ++) {
        	
        	
        Thread.sleep(10000);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode());
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("orders");
        
        int stCnt=jarray.size();
        
        
        System.out.println("Json Array Size  for get call is "+stCnt);
        
        
        int mtchCnt=0;
        
      
        System.out.println(jarray.size());
       
        
        
        
        for(int i=0; i<jarray.size(); i++) {
        	
        	jsonobject = jarray.get(i).getAsJsonObject();
	      
        	//  String statusValidationResult = jsonobject.get("statusCurrent").toString();
	        
	        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
	        
	        System.out.println("statusCurrent Result is " + statusCurrent);
	        
	       
	        
	        if(statusCurrent.equals(statusCurrentValue)) {
	        	res = true;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success for Order ID:"+OrderID);
				
				
				System.out.println("statusCurrent is " +statusCurrent);
				
				//mtchCnt=mtchCnt+1;
				
				
			break;
			
			
	        }
	        
	        else{
	        	res = false;
				utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Failure for Order  ID:"+OrderID);
				System.out.println("statusCurrent is " +statusCurrent);
	        	
	        }

        }
   
   }
    	
} 
    
    
catch (Exception e) {
		// TODO: handle exception
		
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
        
		res = false;
		
		
	}
    
    finally{
        
        response.close();
        
        return res;
        
   }

}










	
	
	
public static boolean GetCallItemStatusValidation_InvalidItems(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid) throws IOException {
	
	boolean res1 = false;
	
	boolean res2=false;
	
	
	boolean res=false;
	
	String [] StatusValues=statusCurrentValue.split(",");
	
	
	int MaxwaitingTime = 60000;
	
	int regularwaitTime = 10000;
	
	CloseableHttpResponse response=null;
    
    CredentialsProvider credsProvider = new BasicCredentialsProvider();
	
    credsProvider.setCredentials(
    		new AuthScope(ProxyHostName, ProxyPort),
    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

    try {
		
    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
        
        RequestConfig config = RequestConfig.custom()
                .setProxy(proxy)
                .build();
        
        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
        httpget.setConfig(config);
        
        httpget.addHeader(AuthorizationKey, AuthorizationValue);
        
        System.out.println("Order ID is " + OrderID);
        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
        
        
        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done");
        
         	
        Thread.sleep(10);
        
        response = httpclient.execute(target, httpget);
        

        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode());
        
        String jsonresponse = EntityUtils.toString(response.getEntity());
        
        
        System.out.println(jsonresponse);
        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
        
      
        
        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
        JsonObject jsonobject = jsonelement.getAsJsonObject();
        
        System.out.println(jsonobject.toString());
        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
        
        JsonArray jarray = jsonobject.getAsJsonArray("items");
        System.out.println(jarray.size());
       
        
        for(int i=0; i<jarray.size(); i++) {
        	
        statusCurrentValue=StatusValues[i+1];
        
        
     
        		 jsonobject = jarray.get(i).getAsJsonObject();
        		
        		
        	//	String itemLineId= jsonobject.get("itemLineId").toString().replaceAll("^\"|\"$", "");

        		   String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");

        		   if(statusCurrent.equals(statusCurrentValue)) {
        		        	
        			   res1=true;
        					
        			   
        			   utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
        					
        					
        					
        					System.out.println("statusCurrent is " +statusCurrent);
        					
        				//continue;
        				
        				
        		        }
        		
        		        
        		        
        		       
        		        else{
        		        	
        		        	res1=false;
        		        	
        		        	
        		        	
        					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrent, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
        					System.out.println("statusCurrent is " +statusCurrent);

        		        }

        }

    	
	} catch (Exception e) {
		// TODO: handle exception
		res1=false;
		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
       
	}
    
    finally{
        
        response.close();
        
        return res1;
        
}
    
  
    
	
}


	
	
	
	
	
public static boolean GetCallItemStatusValidationOnlyConfirmed(String ProxyHostName, int ProxyPort, String SYSUserName, String SYSPassWord, String TargetHostName, int TargetPort, String TargetHeader, String UrlTail, String ApiKey, String AuthorizationKey, String AuthorizationValue, String OrderID, String statusCurrentValue, String tcid) throws IOException {
		
		boolean res = false;
		
		int MaxwaitingTime = 60000;
		
		int regularwaitTime = 10000;
		
		CloseableHttpResponse response=null;
	    
	    CredentialsProvider credsProvider = new BasicCredentialsProvider();
		
	    credsProvider.setCredentials(
	    		new AuthScope(ProxyHostName, ProxyPort),
	    		new UsernamePasswordCredentials(SYSUserName, SYSPassWord));   //put your credentials

	    CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();

	    try {
			
	    	HttpHost target = new HttpHost(TargetHostName, TargetPort, TargetHeader);
	        HttpHost proxy = new HttpHost(ProxyHostName,ProxyPort);
	        
	        RequestConfig config = RequestConfig.custom()
	                .setProxy(proxy)
	                .build();
	        
	        HttpGet httpget = new HttpGet(UrlTail +"/"+ OrderID + "/items/?" + ApiKey);
	        //HttpGet httpget = new HttpGet("/wholesaleorder/v1/customers/amazon/orders/"+OrderID+"?apikey=l6dNLwHbqpnhvUVPqhaLNCjA0qKdFo36");
	        httpget.setConfig(config);
	        
	        httpget.addHeader(AuthorizationKey, AuthorizationValue);
	        
	        System.out.println("Order ID is " + OrderID);
	        utilityFileWriteOP.writeToLog(tcid, "We are searching the status for ", "Order ID "+OrderID);
	        
	        
	        System.out.println("Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy);
	        utilityFileWriteOP.writeToLog(tcid, "Executing request " + httpget.getRequestLine() + " to " + target + " via " + proxy, "Done");
	        
	        for(int waitingTime = 10000; waitingTime<= MaxwaitingTime; waitingTime = waitingTime + 10000) {
	        	
	        	Thread.sleep(regularwaitTime);
	        
	        response = httpclient.execute(target, httpget);
	        

	        System.out.println("Response code is "+response.getStatusLine().getStatusCode());
	        utilityFileWriteOP.writeToLog(tcid, "Response code ", "Displayed is "+response.getStatusLine().getStatusCode());
	        
	        String jsonresponse = EntityUtils.toString(response.getEntity());
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
	        
	      
	        
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
	        
	        JsonArray jarray = jsonobject.getAsJsonArray("items");
	        System.out.println(jarray.size());
	        for(int i=0; i<jarray.size(); i++) {
	        	
	        	jsonobject = jarray.get(i).getAsJsonObject();
		        String statusValidationResult = jsonobject.get("statusValidation").toString();
		        
		        String statusCurrent = jsonobject.get("statusCurrent").toString().replaceAll("^\"|\"$", "");
		        
		        System.out.println("StatusValidation Result is " + statusValidationResult);
		        
		        int countOne = statusValidationResult.split("1").length - 1;
		        
		        if(countOne != 7) {
		        	res = true;
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion is successful ", "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("Count of 1 is not 7");
		        }
		        else {
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "StatusValiadtion Failed ", "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));

		        	break;
		        }
		        
		        if(statusCurrent.equals(statusCurrentValue)) {
		        	res = true;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Success for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("statusCurrent is " +statusCurrentValue);
		        }
		        
		        else{
		        	res = false;
					utilityFileWriteOP.writeToLog(tcid, "StatusCurrent is "+statusCurrentValue, "Failure for Item ID:"+jsonobject.get("itemId").toString().replaceAll("^\"|\"$", ""));
					System.out.println("statusCurrent is " +statusCurrentValue);
		        	break;
		        }
	        	
	        }
	        	  
	        
	        if(res==true) {
	        	break;
	        }
	        
	        }
	    	
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
	        
			res = false;
			return res;
		}
	    
	    finally{
	        
	        response.close();
	        
	}
	    
	    return res;
	    
		
	}

}
