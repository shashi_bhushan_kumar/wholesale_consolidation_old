package Utilities_v1;

import java.util.Arrays;



public class WebPortal_Utilities {
	
	

	public static String[] concatAll(String[] first, String[]... rest)
	{
	  int totalLength = first.length;
	  for (String[] array : rest) 
	  {
	    totalLength += array.length;
	  }
	  
	  String[] result = Arrays.copyOf(first, totalLength);
	  int offset = first.length;
	  
	  for (String[] array : rest)
	  {
	    System.arraycopy(array, 0, result, offset, array.length);
	    offset += array.length;
	  }
	  
	  return result;
	}

}